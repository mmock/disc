#define BOOST_TEST_MODULE Recombination
#include <boost/test/included/unit_test.hpp>
#include "../src/events/JumpEventIrradiation.h"
#include "../src/models/IrradiationModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct RecombinationTest {
  RecombinationTest() { model.parse("configs/Basic.cfg"); }
  virtual ~RecombinationTest(){};
  IrradiationModel model;
};

BOOST_FIXTURE_TEST_CASE(AddAndRemovePointDefect, RecombinationTest) {
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(0, model.vacancyCount());
  model.createVacancy(model.lattice()->sites()[0]);
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  model.createInterstitial(model.lattice()->sites()[10]);
  model.createVacancy(model.lattice()->sites()[20]);
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  BOOST_CHECK_EQUAL(2, model.vacancyCount());
  model.removePointDefect(0);
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  model.removePointDefect(0);
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
}

BOOST_FIXTURE_TEST_CASE(createFailure, RecombinationTest) {
  model.createVacancy(model.lattice()->sites()[0]);
  model.createInterstitial(model.lattice()->sites()[10]);
  BOOST_CHECK_THROW(model.createVacancy(model.lattice()->sites()[0]),
                    std::runtime_error);
  BOOST_CHECK_THROW(model.createInterstitial(model.lattice()->sites()[10]),
                    std::runtime_error);
}

BOOST_FIXTURE_TEST_CASE(RecombinationCheck, RecombinationTest) {
  model.createVacancy(model.lattice()->sites()[0]);
  model.createInterstitial(model.lattice()->sites()[2]);
  model.checkRecombination(model.eventObjects()[0].get());
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(0, model.vacancyCount());

  model.createVacancy(model.lattice()->sites()[0]);
  model.createVacancy(model.lattice()->sites()[2]);
  model.checkRecombination(model.eventObjects()[0].get());
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(2, model.vacancyCount());

  model.removePointDefect(0);
  model.removePointDefect(0);
  model.createInterstitial(model.lattice()->sites()[0]);
  model.createInterstitial(model.lattice()->sites()[2]);
  model.checkRecombination(model.eventObjects()[0].get());
  BOOST_CHECK_EQUAL(2, model.interstitialCount());
  BOOST_CHECK_EQUAL(0, model.vacancyCount());
}

BOOST_FIXTURE_TEST_CASE(MovementAndRecombination, RecombinationTest) {
  // Create vacancy
  model.createVacancy(model.lattice()->sites()[0]);
  EventObject* vacancy = model.eventObjects()[0].get();
  // Create interstitial in center
  model.createInterstitial(
      model.lattice()->sites()[model.lattice()->getIndex({2.5, 2.5, 2.5})]);
  // Move vacancy closer
  JumpEventIrradiation* jump1 = new JumpEventIrradiation(
      &model, vacancy, &model.lattice()->sites()[0],
      &model.lattice()->sites()[model.lattice()->getIndex({0.5, 0.5, 0.5})],
      1.0);
  jump1->execute();
  model.updateModel();
  delete jump1;
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  // and closer
  JumpEventIrradiation* jump2 = new JumpEventIrradiation(
      &model, vacancy,
      &model.lattice()->sites()[model.lattice()->getIndex({0.5, 0.5, 0.5})],
      &model.lattice()->sites()[model.lattice()->getIndex({1.0, 1.0, 1.0})],
      1.0);
  jump2->execute();
  model.updateModel();
  delete jump2;
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  // until recombination
  JumpEventIrradiation* jump3 = new JumpEventIrradiation(
      &model, vacancy,
      &model.lattice()->sites()[model.lattice()->getIndex({1.0, 1.0, 1.0})],
      &model.lattice()->sites()[model.lattice()->getIndex({1.5, 1.5, 1.5})],
      1.0);
  jump3->execute();
  model.updateModel();
  delete jump3;
  BOOST_CHECK_EQUAL(0, model.vacancyCount());
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
}
