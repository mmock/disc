#define BOOST_TEST_MODULE Fe diffusion
#include <boost/test/included/unit_test.hpp>
#include "../src/models/DiffusionModelFe.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct FeDiffusionTest {
  FeDiffusionTest() { model.parse("configs/FeDiffusion.cfg"); }
  virtual ~FeDiffusionTest(){};
  DiffusionModelFe model;
};

BOOST_FIXTURE_TEST_CASE(DefectCoordinateTest, FeDiffusionTest) {
  Vector3 p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0, p[0]);
  BOOST_CHECK_EQUAL(0, p[1]);
  BOOST_CHECK_EQUAL(0, p[2]);
}

BOOST_FIXTURE_TEST_CASE(SimulationTest, FeDiffusionTest) {
  // After 1 step
  model.takeStep();
  Vector3 p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(7.5, p[0]);
  BOOST_CHECK_EQUAL(7.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(2.0432509945007969e-11, model.algorithm()->time(), 0.0001);

  // After 2 steps
  model.takeStep();
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(7.0, p[0]);
  BOOST_CHECK_EQUAL(7.0, p[1]);
  BOOST_CHECK_EQUAL(1.0, p[2]);
  BOOST_CHECK_CLOSE(3.0585842508397779e-11, model.algorithm()->time(), 0.0001);

  // After 5 steps
  while (model.algorithm()->step() < 5) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(6.5, p[0]);
  BOOST_CHECK_EQUAL(6.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(1.3533239754968653e-10, model.algorithm()->time(), 0.0001);

  // After 10 steps
  while (model.algorithm()->step() < 10) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0, p[0]);
  BOOST_CHECK_EQUAL(5, p[1]);
  BOOST_CHECK_EQUAL(0, p[2]);
  BOOST_CHECK_CLOSE(2.1107293547222779e-10, model.algorithm()->time(), 0.0001);
}
