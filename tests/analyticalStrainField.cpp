#define BOOST_TEST_MODULE AnalyticalStrainfield
#include "../src/fields/AnalyticalStrainField.h"
#include <boost/test/included/unit_test.hpp>
#include "../src/models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct AnalyticalStrainFieldTest {
  AnalyticalStrainFieldTest() {
    model.parse("configs/AnalyticalStrainField.cfg");
    field = static_cast<AnalyticalStrainField*>(model.strainField().get());
  }
  StrainModel model;
  AnalyticalStrainField* field;
};

BOOST_FIXTURE_TEST_CASE(SphericalCoordinates, AnalyticalStrainFieldTest) {
  Vector3 coord;
  coord = field->sphericalCoordinates({7, 5, 5});  // (2, 0, pi/2)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(0.0, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(M_PI / 2.0, coord[2], 0.0001);

  coord = field->sphericalCoordinates({3, 5, 5});  // (2, pi, pi/2)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(M_PI, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(M_PI / 2.0, coord[2], 0.0001);

  coord = field->sphericalCoordinates({5, 7, 5});  // (2, pi/2, pi/2)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(M_PI / 2.0, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(M_PI / 2.0, coord[2], 0.0001);

  coord = field->sphericalCoordinates({5, 3, 5});  // (2, -pi/2, pi/2)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(-M_PI / 2.0, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(M_PI / 2.0, coord[2], 0.0001);

  coord = field->sphericalCoordinates({5, 5, 7});  // (2, 0, 0)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(0.0, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(0.0, coord[2], 0.0001);

  coord = field->sphericalCoordinates({5, 5, 3});  // (2, 0, pi)
  BOOST_CHECK_CLOSE(2.0, coord[0], 0.0001);
  BOOST_CHECK_CLOSE(0.0, coord[1], 0.0001);
  BOOST_CHECK_CLOSE(M_PI, coord[2], 0.0001);
}

BOOST_FIXTURE_TEST_CASE(cartesianTensor, AnalyticalStrainFieldTest) {
  TensorFull cart;
  TensorFull sphe;
  sphe << 1.0, 0.0, 0.0, 0.0, 2.0, 0.0, 0.0, 0.0, 2.0;
  cart = field->cartesianTensor({2, 0, 0}, sphe);
  BOOST_CHECK_CLOSE(2.0, cart(0), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(1), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(2), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(3), 0.0001);
  BOOST_CHECK_CLOSE(2.0, cart(4), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(5), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(6), 0.0001);
  BOOST_CHECK_CLOSE(0.0, cart(7), 0.0001);
  BOOST_CHECK_CLOSE(1.0, cart(8), 0.0001);
}