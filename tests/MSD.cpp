#define BOOST_TEST_MODULE MSD
#include <boost/test/included/unit_test.hpp>
#include "../src/models/DiffusionModelFe.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct MSDTest {
  MSDTest() {
    model.parse("configs/msd.cfg");
    // write initial configuration
    model.writeResults();
    while (model.algorithm()->step() < model.algorithm()->maximumSteps()) {
      model.takeStep();
    }
    // Write results
    for (auto& analyzer : model.analyzer()) {
      analyzer->writeResults();
    }
  }

  virtual ~MSDTest(){};
  DiffusionModelFe model;
};

BOOST_FIXTURE_TEST_CASE(MSD, MSDTest) {
  std::ifstream ifs1("reference/msd.ref");
  std::ifstream ifs2("results/msd.data");

  std::istream_iterator<char> b1(ifs1), e1;
  std::istream_iterator<char> b2(ifs2), e2;

  BOOST_CHECK_EQUAL_COLLECTIONS(b1, e1, b2, e2);
}
