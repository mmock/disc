#define BOOST_TEST_MODULE Strain diffusion
#include <boost/test/included/unit_test.hpp>
#include "../src/models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct StrainDiffusionTest {
  StrainDiffusionTest() { model.parse("configs/StrainField.cfg"); }
  virtual ~StrainDiffusionTest(){};
  StrainModel model;
};

BOOST_FIXTURE_TEST_CASE(CalculateVacancyJumpRate, StrainDiffusionTest) {
  Site& p0 =
      model.lattice()->sites()[model.lattice()->getIndex({0.0, 0.0, 0.0})];
  Site& p1 =
      model.lattice()->sites()[model.lattice()->getIndex({0.5, 0.5, 0.5})];
  Site& p2 =
      model.lattice()->sites()[model.lattice()->getIndex({1.0, 1.0, 1.0})];
  BOOST_CHECK_CLOSE(7.2240190489833207, model.vacancyJumpRate(p0, p1), 0.0001);
  BOOST_CHECK_CLOSE(7.2240092653712109, model.vacancyJumpRate(p1, p2), 0.0001);
  BOOST_CHECK_CLOSE(7.2239233947480637, model.vacancyJumpRate(p2, p1), 0.0001);
  BOOST_CHECK_CLOSE(7.2239331782438763, model.vacancyJumpRate(p1, p0), 0.0001);
}

BOOST_FIXTURE_TEST_CASE(VacancyDiffusion, StrainDiffusionTest) {
  model.createVacancy(
      model.lattice()->sites()[model.lattice()->getIndex({0.0, 0.0, 0.0})]);
  BOOST_CHECK_EQUAL(0, model.algorithm()->time());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  for (int i = 0; i < 100; ++i) {
    model.takeStep();
  }
  BOOST_CHECK_CLOSE(1.6196603325503256, model.algorithm()->time(), 0.0001);
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(4, model.eventObjects()[0]->position()[0]);
  BOOST_CHECK_EQUAL(1, model.eventObjects()[0]->position()[1]);
  BOOST_CHECK_EQUAL(4, model.eventObjects()[0]->position()[2]);
}

BOOST_FIXTURE_TEST_CASE(InterstitialDiffusion, StrainDiffusionTest) {
  model.createInterstitial(
      model.lattice()->sites()[model.lattice()->getIndex({0.0, 0.0, 0.0})]);
  BOOST_CHECK_EQUAL(0, model.algorithm()->time());
  BOOST_CHECK_EQUAL(0, model.vacancyCount());
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  for (int i = 0; i < 100; ++i) {
    model.takeStep();
  }
  BOOST_CHECK_CLOSE(0.00015052235644400308, model.algorithm()->time(), 0.0001);
  BOOST_CHECK_EQUAL(0, model.vacancyCount());
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  BOOST_CHECK_EQUAL(4, model.eventObjects()[0]->position()[0]);
  BOOST_CHECK_EQUAL(1, model.eventObjects()[0]->position()[1]);
  BOOST_CHECK_EQUAL(4, model.eventObjects()[0]->position()[2]);
}
