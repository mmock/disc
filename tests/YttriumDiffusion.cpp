#define BOOST_TEST_MODULE Yttrium diffusion
#include <boost/test/included/unit_test.hpp>
#include "../src/models/DiffusionModelYttrium.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct YttriumDiffusionTest {
  YttriumDiffusionTest() { model.parse("configs/YttriumDiffusion.cfg"); }
  virtual ~YttriumDiffusionTest(){};
  DiffusionModelYttrium model;
};

BOOST_FIXTURE_TEST_CASE(RateTest, YttriumDiffusionTest) {
  model.updateEvents();
  auto e = model.events();
  // 1 NN position:
  double r12 = 1503.3906734788618;
  double r13 = 5068937.4765012655;
  double r15 = 45970040.398356952;
  BOOST_CHECK_EQUAL(14, e.size());
  BOOST_CHECK_CLOSE(r15, e[0]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[1]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[2]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[3]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[4]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[5]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[6]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[7]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[8]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[9]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r12, e[10]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[11]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r13, e[12]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r15, e[13]->getRate(), 0.0001);

  e[3]->execute();
  model.updateModel();
  model.updateEvents();
  e = model.events();
  // 2 NN position
  double r21 = 146707647.92307675;
  double r24 = 2116419980.6663842;
  BOOST_CHECK_EQUAL(8, e.size());
  BOOST_CHECK_CLOSE(r24, e[0]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r24, e[1]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r24, e[2]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r24, e[3]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r21, e[4]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r21, e[5]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r21, e[6]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r21, e[7]->getRate(), 0.0001);

  e[4]->execute();
  model.updateModel();
  model.updateEvents();
  e = model.events();
  // 1 NN position
  BOOST_CHECK_EQUAL(14, e.size());
  e[1]->execute();
  model.updateModel();
  model.updateEvents();
  e = model.events();
  // 3 NN position
  double r31 = 883663204631.9115;
  double r30 = 3366611484.0880842;
  BOOST_CHECK_EQUAL(8, e.size());
  BOOST_CHECK_CLOSE(r30, e[0]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r30, e[1]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r30, e[2]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r30, e[3]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r30, e[4]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r30, e[5]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r31, e[6]->getRate(), 0.0001);
  BOOST_CHECK_CLOSE(r31, e[7]->getRate(), 0.0001);
}

BOOST_FIXTURE_TEST_CASE(SimulationTest, YttriumDiffusionTest) {
  // After 1 step
  model.takeStep();
  Vector3 p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0.5, p[0]);
  BOOST_CHECK_EQUAL(0.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(6.37013921360534e-09, model.algorithm()->time(), 0.0001);

  // After 2 steps
  model.takeStep();
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0.25, p[0]);
  BOOST_CHECK_EQUAL(0.25, p[1]);
  BOOST_CHECK_EQUAL(0.25, p[2]);
  BOOST_CHECK_CLOSE(6.3701874045005866e-09, model.algorithm()->time(), 0.0001);

  // After 10 steps
  while (model.algorithm()->step() < 10) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0.25, p[0]);
  BOOST_CHECK_EQUAL(0.25, p[1]);
  BOOST_CHECK_EQUAL(0.25, p[2]);
  BOOST_CHECK_CLOSE(4.6409454921331218e-08, model.algorithm()->time(), 0.0001);

  // After 1000 steps
  while (model.algorithm()->step() < 1000) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(7.75, p[0]);
  BOOST_CHECK_EQUAL(7.75, p[1]);
  BOOST_CHECK_EQUAL(7.25, p[2]);
  BOOST_CHECK_CLOSE(3.6477677345615275e-06, model.algorithm()->time(), 0.0001);
}
