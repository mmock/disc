#define BOOST_TEST_MODULE Vacancy
#include <boost/test/included/unit_test.hpp>
#include "../src/events/JumpEvent.h"
#include "../src/models/IrradiationModel.h"
#include "../src/objects/EventObject.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct VacancyTest {
  VacancyTest() { model.parse("configs/Basic.cfg"); }
  virtual ~VacancyTest(){};
  IrradiationModel model;
};

BOOST_FIXTURE_TEST_CASE(Positions, VacancyTest) {
  model.createVacancy(model.lattice()->sites()[1]);
  EventObject* vacancy = model.eventObjects()[0].get();
  JumpEvent* jump =
      new JumpEvent(&model.lattice()->sites()[1], &model.lattice()->sites()[0], 1.0);
  Vector3 ps = jump->start().coord();
  BOOST_CHECK_EQUAL(0.5, ps[0]);
  BOOST_CHECK_EQUAL(0.5, ps[1]);
  BOOST_CHECK_EQUAL(0.5, ps[2]);
  Vector3 pv = vacancy->site().coord();
  BOOST_CHECK_EQUAL(0.5, pv[0]);
  BOOST_CHECK_EQUAL(0.5, pv[1]);
  BOOST_CHECK_EQUAL(0.5, pv[2]);
  Vector3 pt = jump->target().coord();
  BOOST_CHECK_EQUAL(0.0, pt[0]);
  BOOST_CHECK_EQUAL(0.0, pt[1]);
  BOOST_CHECK_EQUAL(0.0, pt[2]);
  BOOST_CHECK_EQUAL(1, jump->start().atomIndex());
  BOOST_CHECK_EQUAL(0, jump->target().atomIndex());
  jump->execute();
  ps = jump->start().coord();
  BOOST_CHECK_EQUAL(0.5, ps[0]);
  BOOST_CHECK_EQUAL(0.5, ps[1]);
  BOOST_CHECK_EQUAL(0.5, ps[2]);
  pv = vacancy->site().coord();
  BOOST_CHECK_EQUAL(0.0, pv[0]);
  BOOST_CHECK_EQUAL(0.0, pv[1]);
  BOOST_CHECK_EQUAL(0.0, pv[2]);
  pt = jump->target().coord();
  BOOST_CHECK_EQUAL(0.0, pt[0]);
  BOOST_CHECK_EQUAL(0.0, pt[1]);
  BOOST_CHECK_EQUAL(0.0, pt[2]);
  BOOST_CHECK_EQUAL(0, jump->start().atomIndex());
  BOOST_CHECK_EQUAL(1, jump->target().atomIndex());
  delete jump;
}

BOOST_FIXTURE_TEST_CASE(Movement, VacancyTest) {
  // Create vacancy
  model.createVacancy(model.lattice()->sites()[1]);
  EventObject* vacancy = model.eventObjects()[0].get();
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(1, model.lattice()->sites()[1].atomIndex());
  BOOST_CHECK_EQUAL(0, model.lattice()->sites()[0].atomIndex());
  // Check vacancy position
  Vector3 pv = vacancy->site().coord();
  BOOST_CHECK_EQUAL(0.5, pv[0]);
  BOOST_CHECK_EQUAL(0.5, pv[1]);
  BOOST_CHECK_EQUAL(0.5, pv[2]);

  // Move that vacancy
  JumpEvent* jump =
      new JumpEvent(&model.lattice()->sites()[1], &model.lattice()->sites()[0], 1.0);
  jump->execute();
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_EQUAL(0, model.lattice()->sites()[1].atomIndex());
  BOOST_CHECK_EQUAL(1, model.lattice()->sites()[0].atomIndex());
  // Check vacancy position
  pv = vacancy->site().coord();
  BOOST_CHECK_EQUAL(0, pv[0]);
  BOOST_CHECK_EQUAL(0, pv[1]);
  BOOST_CHECK_EQUAL(0, pv[2]);
  delete jump;
}