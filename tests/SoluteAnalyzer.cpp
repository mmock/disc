#define BOOST_TEST_MODULE Solute analyzer
#include <boost/test/included/unit_test.hpp>
#include "../src/models/DiffusionModelSolute.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct SoluteAnalyzerTest {
  SoluteAnalyzerTest() {
    model.parse("configs/SoluteAnalyzer.cfg");
    // write initial configuration
    model.writeResults();
    while (model.algorithm()->step() < model.algorithm()->maximumSteps()) {
      model.takeStep();
    }
    // Write results
    for (auto& analyzer : model.analyzer()) {
      analyzer->writeResults();
    }
  }

  virtual ~SoluteAnalyzerTest(){};
  DiffusionModelSolute model;
};

BOOST_FIXTURE_TEST_CASE(Output, SoluteAnalyzerTest) {
  std::ifstream refJumps("reference/jumps.ref");
  std::ifstream resJumps("results/jumps.data");

  std::istream_iterator<char> b1(refJumps), e1;
  std::istream_iterator<char> b2(resJumps), e2;

  BOOST_CHECK_EQUAL_COLLECTIONS(b1, e1, b2, e2);

  std::ifstream refPos("reference/position.ref");
  std::ifstream resPos("results/position.data");

  std::istream_iterator<char> b3(refPos), e3;
  std::istream_iterator<char> b4(resPos), e4;

  BOOST_CHECK_EQUAL_COLLECTIONS(b3, e3, b4, e4);
}
