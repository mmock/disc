#define BOOST_TEST_MODULE Solute diffusion
#include <boost/test/included/unit_test.hpp>
#include "../src/models/DiffusionModelSolute.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct SoluteDiffusionTest {
  SoluteDiffusionTest() { model.parse("configs/SoluteDiffusion.cfg"); }
  virtual ~SoluteDiffusionTest(){};
  DiffusionModelSolute model;
};

BOOST_FIXTURE_TEST_CASE(SimulationTest, SoluteDiffusionTest) {
  // After 1 step
  model.takeStep();
  Vector3 p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0, p[0]);
  BOOST_CHECK_EQUAL(0, p[1]);
  BOOST_CHECK_EQUAL(0, p[2]);
  BOOST_CHECK_CLOSE(1.0418331061243397e-11, model.algorithm()->time(), 0.0001);

  // After 2 steps
  model.takeStep();
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0.5, p[0]);
  BOOST_CHECK_EQUAL(0.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(1.559541308909981e-11, model.algorithm()->time(), 0.0001);

  // After 10 steps
  while (model.algorithm()->step() < 10) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(0.5, p[0]);
  BOOST_CHECK_EQUAL(0.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(1.0555851566966158e-10, model.algorithm()->time(), 0.0001);

  // After 1000 steps
  while (model.algorithm()->step() < 1000) {
    model.takeStep();
  }
  p = model.defectCoordinate();
  BOOST_CHECK_EQUAL(7.5, p[0]);
  BOOST_CHECK_EQUAL(0.5, p[1]);
  BOOST_CHECK_EQUAL(0.5, p[2]);
  BOOST_CHECK_CLOSE(2.0717967780194974e-08, model.algorithm()->time(), 0.0001);
}
