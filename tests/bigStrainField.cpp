#define BOOST_TEST_MODULE BigStrainfield
#include <boost/test/included/unit_test.hpp>
#include "../src/fields/StrainField.h"
#include "../src/models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct BigStrainFieldTest {
  BigStrainFieldTest() {
    model.parse("configs/BigStrainField.cfg");
    field = model.strainField().get();
  }
  StrainModel model;
  StrainField* field;
};

BOOST_FIXTURE_TEST_CASE(CalculateStrain, BigStrainFieldTest) {
  Tensor result = field->calculateStrain({0, 0, 0});
  BOOST_CHECK_CLOSE(-2.505223256e-06, result[0], 0.1);
  BOOST_CHECK_CLOSE(-2.498775411e-06, result[1], 0.1);
  BOOST_CHECK_CLOSE(-2.502002095e-06, result[2], 0.1);
  BOOST_CHECK_CLOSE(-9.427561902e-08, result[3], 0.1);
  BOOST_CHECK_CLOSE(-1.037077391e-07, result[4], 0.1);
  BOOST_CHECK_CLOSE(-1.017680746e-07, result[5], 0.1);

  result = field->calculateStrain({11, 32, 26});
  BOOST_CHECK_CLOSE(-1.048198945e-07, result[0], 0.4);  // Warum?!
  BOOST_CHECK_CLOSE(-9.772522072e-06, result[1], 0.1);
  BOOST_CHECK_CLOSE(4.614401513e-06, result[2], 0.1);
  BOOST_CHECK_CLOSE(-7.525722102e-06, result[3], 0.1);
  BOOST_CHECK_CLOSE(3.969443928e-06, result[4], 0.1);
  BOOST_CHECK_CLOSE(1.1987996e-05, result[5], 0.1);

  result = field->calculateStrain({33, 8, 36});
  BOOST_CHECK_CLOSE(-1.1835254e-06, result[0], 0.1);
  BOOST_CHECK_CLOSE(-6.925689681e-07, result[1], 0.1);
  BOOST_CHECK_CLOSE(-3.951260989e-06, result[2], 0.1);
  BOOST_CHECK_CLOSE(2.925661064e-06, result[3], 0.1);
  BOOST_CHECK_CLOSE(-3.199748939e-06, result[4], 0.1);
  BOOST_CHECK_CLOSE(2.303991296e-06, result[5], 0.1);

  result = field->calculateStrain({2.5, 12.5, 36.5});
  BOOST_CHECK_CLOSE(-3.662236e-06, result[0], 0.1);
  BOOST_CHECK_CLOSE(1.234344568e-06, result[1], 0.1);
  BOOST_CHECK_CLOSE(-3.125194728e-06, result[2], 0.1);
  BOOST_CHECK_CLOSE(6.026002782e-07, result[3], 0.1);
  BOOST_CHECK_CLOSE(1.558201453e-06, result[4], 0.1);
  BOOST_CHECK_CLOSE(-6.043901905e-07, result[5], 0.1);

  result = field->calculateStrain({11.5, 1.5, 2.5});
  BOOST_CHECK_CLOSE(9.992346803e-07, result[0], 0.1);
  BOOST_CHECK_CLOSE(-3.697315518e-06, result[1], 0.1);
  BOOST_CHECK_CLOSE(-3.193610426e-06, result[2], 0.1);
  BOOST_CHECK_CLOSE(-6.164197571e-07, result[3], 0.1);
  BOOST_CHECK_CLOSE(-4.016989476e-07, result[4], 0.1);
  BOOST_CHECK_CLOSE(-2.93029275e-07, result[5], 0.1);
}
