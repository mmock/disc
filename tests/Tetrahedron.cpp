#define BOOST_TEST_MODULE Tetrahedron
#include "../src/fields/Tetrahedron.h"
#include <boost/test/included/unit_test.hpp>

struct TetrahedronTest {
  TetrahedronTest() {
    Vector3 n1 = {-0.5, -0.5, -0.5};
    Vector3 n2 = {0.5, -0.5, -0.5};
    Vector3 n3 = {0.5, 0.5, -0.5};
    Vector3 n4 = {0, 0, 0};

    tet = Tetrahedron({0, 1, 2, 8}, {n1, n2, n3, n4});
  }
  virtual ~TetrahedronTest(){};
  Tetrahedron tet;
};

BOOST_FIXTURE_TEST_CASE(Indices, TetrahedronTest) {
  BOOST_CHECK_EQUAL(0, tet.nodeIndices()[0]);
  BOOST_CHECK_EQUAL(1, tet.nodeIndices()[1]);
  BOOST_CHECK_EQUAL(2, tet.nodeIndices()[2]);
  BOOST_CHECK_EQUAL(8, tet.nodeIndices()[3]);
}

BOOST_FIXTURE_TEST_CASE(InternalCoordinates, TetrahedronTest) {
  // Ecken
  Vector3 p = {-0.5, -0.5, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {0.5, -0.5, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 1);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {0.5, 0.5, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 1);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {0, 0, 0};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 1);
  // Kanten
  p = {0.0, -0.5, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0.5);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {0.25, 0.25, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0.75);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {-0.375, -0.375, -0.375};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0.25);
  // Außerhalb
  p = {0.5, 0.5, 0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 2);
  p = {-1.5, -0.5, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], -1);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
  p = {-1, -1, -0.5};
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[0], 0);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[1], -0.5);
  BOOST_CHECK_EQUAL(tet.internalCoordinates(p)[2], 0);
}

BOOST_FIXTURE_TEST_CASE(BoundingBox, TetrahedronTest) {
  Vector3 p = {0, 0, 0};
  BOOST_CHECK(tet.inBoundingBox(p));
  p = {-0.5, -0.5, -0.5};
  BOOST_CHECK(tet.inBoundingBox(p));
  p = {0.5, -0.5, -0.5};
  BOOST_CHECK(tet.inBoundingBox(p));
  p = {0.5, 0.5, -0.5};
  BOOST_CHECK(tet.inBoundingBox(p));
  p = {0.4, -0.3, -0.5};
  BOOST_CHECK(tet.inBoundingBox(p));
  p = {1.0, 0.0, 0.25};
  BOOST_CHECK(!tet.inBoundingBox(p));
  p = {0.0, -1.0, 0.25};
  BOOST_CHECK(!tet.inBoundingBox(p));
  p = {0.0, 0.0, 0.75};
  BOOST_CHECK(!tet.inBoundingBox(p));
}

BOOST_FIXTURE_TEST_CASE(FormFunction, TetrahedronTest) {
  double sum = 0;
  auto form = tet.formFunction({0, 0, 0});
  for (auto& n : form) sum += n;
  BOOST_CHECK_CLOSE(sum, 1, 0.0001);
  sum = 0;
  form = tet.formFunction({0.5, 0.5, -0.5});
  for (auto& n : form) sum += n;
  BOOST_CHECK_CLOSE(sum, 1, 0.0001);
  sum = 0;
  form = tet.formFunction({0.4, -0.3, -0.5});
  for (auto& n : form) sum += n;
  BOOST_CHECK_CLOSE(sum, 1, 0.0001);
}
