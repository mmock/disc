#define BOOST_TEST_MODULE Lattice
#include "../src/lattice/Lattice.h"
#include <boost/test/included/unit_test.hpp>

struct LatticeTest {
  LatticeTest() {
    p1 << 1, 2, 3;
    p2 << 2, 2, 2;
    p3 << 0, 0, 0;
    p4 << 5, 5, 5;
    p5 << 0, 0, -1;
    p6 << -2, -2, -2;
    cell << 5, 5, 5;
  }
  Vector3 p1, p2, p3, p4, p5, p6, cell;
};

BOOST_FIXTURE_TEST_CASE(Distance, LatticeTest) {
  // // Without MIC
  // BOOST_CHECK_CLOSE(sqrt(2), calculateDistance(p1, p2), 0.0001);
  // BOOST_CHECK_CLOSE(sqrt(75), calculateDistance(p3, p4), 0.0001);
  // BOOST_CHECK_CLOSE(1.0, calculateDistance(p3, p5), 0.0001);
  // BOOST_CHECK_CLOSE(sqrt(86), calculateDistance(p4, p5), 0.0001);
  // BOOST_CHECK_CLOSE(sqrt(29), calculateDistance(p1, p4), 0.0001);

  // // With MIC
  // BOOST_CHECK_CLOSE(sqrt(2), calculateDistance(p1, p2, cell), 0.0001);
  // BOOST_CHECK_CLOSE(0.0, calculateDistance(p3, p4, cell), 0.0001);
  // BOOST_CHECK_CLOSE(1.0, calculateDistance(p3, p5, cell), 0.0001);
  // BOOST_CHECK_CLOSE(1.0, calculateDistance(p4, p5, cell), 0.0001);
  // BOOST_CHECK_CLOSE(3.0, calculateDistance(p1, p4, cell), 0.0001);
}

BOOST_FIXTURE_TEST_CASE(WrapInCell, LatticeTest) {
  // wrapInCell(p4, cell);
  // BOOST_CHECK_CLOSE(0.0, p4[0], 0.0001);
  // BOOST_CHECK_CLOSE(0.0, p4[1], 0.0001);
  // BOOST_CHECK_CLOSE(0.0, p4[2], 0.0001);

  // wrapInCell(p5, cell);
  // BOOST_CHECK_CLOSE(0.0, p5[0], 0.0001);
  // BOOST_CHECK_CLOSE(0.0, p5[1], 0.0001);
  // BOOST_CHECK_CLOSE(4.0, p5[2], 0.0001);

  // wrapInCell(p6, cell);
  // BOOST_CHECK_CLOSE(3.0, p6[0], 0.0001);
  // BOOST_CHECK_CLOSE(3.0, p6[1], 0.0001);
  // BOOST_CHECK_CLOSE(3.0, p6[2], 0.0001);
}