#define BOOST_TEST_MODULE Irradiation
#include <boost/test/included/unit_test.hpp>
#include "../src/models/IrradiationModel.h"
#include "../src/objects/IrradiationObject.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct IrradiationTest {
  IrradiationTest() { model.parse("configs/Irradiation.cfg"); }
  virtual ~IrradiationTest(){};
  IrradiationModel model;
};

BOOST_FIXTURE_TEST_CASE(checkRate, IrradiationTest) {
  auto source = static_cast<IrradiationObject<IrradiationModel>*>(
      model.eventObjects()[0].get());
  BOOST_CHECK_CLOSE(0.25, source->dose(), 0.0001);
}

BOOST_FIXTURE_TEST_CASE(spawnPositions, IrradiationTest) {
  model.takeStep();
  EventObject* interstitial = model.eventObjects()[1].get();
  EventObject* vacancy = model.eventObjects()[2].get();
  BOOST_CHECK_EQUAL(Interstitial, interstitial->type());
  BOOST_CHECK_EQUAL(Vacancy, vacancy->type());

  // Check interstitial position
  Vector3 pi = interstitial->site().coord();
  BOOST_CHECK_EQUAL(3, pi[0]);
  BOOST_CHECK_EQUAL(3, pi[1]);
  BOOST_CHECK_EQUAL(4, pi[2]);

  // Check vacancy position
  Vector3 pv = vacancy->site().coord();
  BOOST_CHECK_EQUAL(2, pv[0]);
  BOOST_CHECK_EQUAL(1, pv[1]);
  BOOST_CHECK_EQUAL(2, pv[2]);
}

BOOST_FIXTURE_TEST_CASE(simulationTest, IrradiationTest) {
  BOOST_CHECK_EQUAL(0, model.interstitialCount());
  BOOST_CHECK_EQUAL(0, model.vacancyCount());

  // After 1 step
  model.takeStep();
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  BOOST_CHECK_CLOSE(6.0754797234408207, model.algorithm()->time(), 0.0001);

  // After 1000 steps
  while (model.algorithm()->step() < 1000) {
    model.takeStep();
  }
  BOOST_CHECK_EQUAL(1, model.interstitialCount());
  BOOST_CHECK_EQUAL(1, model.vacancyCount());
  // BOOST_CHECK_CLOSE(964.33246055256393, model.algorithm()->time(), 0.0001);
  // BOOST_CHECK_CLOSE(692.81293129215783, model.algorithm()->time(), 0.0001);
  BOOST_CHECK_CLOSE(620.39040094968129, model.algorithm()->time(), 0.0001);
  // Should irradiation event only produce pairs in distance > frenkel pair
  // radius?
}