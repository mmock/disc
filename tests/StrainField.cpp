#define BOOST_TEST_MODULE Strainfield
#include "../src/fields/StrainField.h"
#include <boost/test/included/unit_test.hpp>
#include "../src/models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = none;

struct StrainFieldTest {
  StrainFieldTest() {
    model.parse("configs/StrainField.cfg");
    field = model.strainField().get();
  }
  StrainModel model;
  StrainField* field;
};

BOOST_FIXTURE_TEST_CASE(GetStrain, StrainFieldTest) {
  auto& sites = model.lattice()->sites();
  // (0,0,0)
  BOOST_CHECK_CLOSE(1.700665e-08, field->strain(sites[0])[0], 0.0001);
  BOOST_CHECK_CLOSE(1.245502e-08, field->strain(sites[0])[1], 0.0001);
  BOOST_CHECK_CLOSE(2.528133e-08, field->strain(sites[0])[2], 0.0001);
  BOOST_CHECK_CLOSE(-5.1498529999999999e-08, field->strain(sites[0])[3], 0.0001);
  BOOST_CHECK_CLOSE(-4.3763489999999997e-08, field->strain(sites[0])[4], 0.0001);
  BOOST_CHECK_CLOSE(-5.896926e-08, field->strain(sites[0])[5], 0.0001);

  // (2.5, 2.5, 2.5)
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[0], 0.0001);
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[1], 0.0001);
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[2], 0.0001);
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[3], 0.0001);
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[4], 0.0001);
  BOOST_CHECK_CLOSE(0, field->strain(sites[125])[5], 0.0001);

  auto& site = sites[model.lattice()->getIndex({1.0, 1.0, 1.0})];
  auto& neighbor = sites[model.lattice()->getIndex({1.5, 1.5, 1.5})];
  Tensor saddle = field->saddleStrain(site, &neighbor);
  BOOST_CHECK_CLOSE(8.5033249999999995e-09, saddle[0], 0.0001);
  BOOST_CHECK_CLOSE(6.22751e-09, saddle[1], 0.0001);
  BOOST_CHECK_CLOSE(1.2640665e-08, saddle[2], 0.0001);
  BOOST_CHECK_CLOSE(-2.5749265e-08, saddle[3], 0.0001);
  BOOST_CHECK_CLOSE(-2.1881744999999998e-08, saddle[4], 0.0001);
  BOOST_CHECK_CLOSE(-2.948463e-08, saddle[5], 0.0001);
}

BOOST_FIXTURE_TEST_CASE(CalculateStrain, StrainFieldTest) {
  Vector3 cell = model.lattice()->cellSize();
  Tensor result = field->calculateStrain({0, 0, 0});
  BOOST_CHECK_CLOSE(1.700665e-08, result[0], 0.0001);
  BOOST_CHECK_CLOSE(1.245502e-08, result[1], 0.0001);
  BOOST_CHECK_CLOSE(2.528133e-08, result[2], 0.0001);
  BOOST_CHECK_CLOSE(-5.1498529999999999e-08, result[3], 0.0001);
  BOOST_CHECK_CLOSE(-4.3763489999999997e-08, result[4], 0.0001);
  BOOST_CHECK_CLOSE(-5.896926e-08, result[5], 0.0001);

  result = field->calculateStrain({2.5, 2.5, 2.5});
  BOOST_CHECK_CLOSE(0, result[0], 0.0001);
  BOOST_CHECK_CLOSE(0, result[1], 0.0001);
  BOOST_CHECK_CLOSE(0, result[2], 0.0001);
  BOOST_CHECK_CLOSE(0, result[3], 0.0001);
  BOOST_CHECK_CLOSE(0, result[4], 0.0001);
  BOOST_CHECK_CLOSE(0, result[5], 0.0001);

  result = field->calculateStrain({1.25, 1.25, 1.25});
  BOOST_CHECK_CLOSE(8.5033249999999995e-09, result[0], 0.0001);
  BOOST_CHECK_CLOSE(6.22751e-09, result[1], 0.0001);
  BOOST_CHECK_CLOSE(1.2640665e-08, result[2], 0.0001);
  BOOST_CHECK_CLOSE(-2.5749265e-08, result[3], 0.0001);
  BOOST_CHECK_CLOSE(-2.1881744999999998e-08, result[4], 0.0001);
  BOOST_CHECK_CLOSE(-2.948463e-08, result[5], 0.0001);
}
