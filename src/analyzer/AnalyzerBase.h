///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Analyzer.h"

template <class Model>
class AnalyzerBase : public Analyzer {
 public:
  AnalyzerBase(libconfig::Setting& cfg, Model* model)
      : Analyzer(), _cfg(cfg), _model(model) {}
  virtual ~AnalyzerBase() = default;

  const std::string& resultFile() const { return _resultFile; }
  const libconfig::Setting& config() const { return _cfg; }

 protected:
  void parse() {
    _resultFile = parseString(_cfg, "FileName");
    MsgLogger(normal) << "FileName: " << _resultFile << std::endl;
  }

 protected:
  Model* _model;
  libconfig::Setting& _cfg;
  std::string _resultFile;
};
