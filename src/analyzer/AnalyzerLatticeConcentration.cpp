///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerLatticeConcentration.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../events/JumpEventIrradiation.h"
#include "../models/IrradiationModel.h"

AnalyzerLatticeConcentration::AnalyzerLatticeConcentration(
    IrradiationModel* model, libconfig::Setting& cfg)
    : AnalyzerBase<IrradiationModel>(cfg, model) {
  parse();

  _vacTime.resize(_model->lattice()->numSites());
  _intTime.resize(_model->lattice()->numSites());
}

void AnalyzerLatticeConcentration::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing spheric point defect concentration analyzer:"
                    << std::endl;
  AnalyzerBase::parse();

  _waitTime = parseDouble(config(), "WaitTime");
  MsgLogger(normal) << "WaitTime: " << _waitTime << std::endl;
}


void AnalyzerLatticeConcentration::registerStep() {
  if (_model->algorithm()->time() < _waitTime) return;

  double stepDuration = _model->algorithm()->stepDuration();
  _measurementTime += stepDuration;

  for (auto& object : _model->eventObjects()) {
    if (object->type() == Vacancy) {
      _vacTime[object->site().siteIndex()] += stepDuration;
    } 
    else if (object->type() == Interstitial) {
      _intTime[object->site().siteIndex()] += stepDuration;
    }
  }
}

void AnalyzerLatticeConcentration::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug)
      << "Writing lattice concentration analyzer results to: '"
      << resultFile() << "'" << std::endl;

  if (_measurementTime == 0) {
    MsgLogger(normal) << "Warning: Lattice concentration analyzer hasn't "
                         "started measuring yet!"
                      << std::endl;
  }

  std::ofstream resultsFile;
  resultsFile.open(resultFile());
  size_t n = _model->lattice()->numSites();
  resultsFile << n << "\n";
  resultsFile << "# x y z vacancyTime interstitialTime\n";
  for (size_t i = 0; i < n; ++i) {
    auto& pos = _model->lattice()->sites()[i].coord();
    resultsFile << pos[0] << " " << pos[1] << " " << pos[2] << " " 
                << _vacTime[i] / _measurementTime << " " 
                << _intTime[i] / _measurementTime << std::endl;
  }
  resultsFile.close();
}
