///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerJumpCount.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../models/DiffusionModelSolute.h"

AnalyzerJumpCount::AnalyzerJumpCount(DiffusionModelSolute* model,
                                     libconfig::Setting& cfg)
    : AnalyzerBase<DiffusionModelSolute>(cfg, model) {
  parse();
  for (size_t start = 0; start < 10; ++start) {
    for (size_t end = 0; end < 10; ++end) {
      _jumps[start][end] = 0;
    }
  }
  _previousNeighborShell = _model->neighborShell();
}

void AnalyzerJumpCount::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing jump count analyzer:" << std::endl;
  AnalyzerBase::parse();
}

void AnalyzerJumpCount::registerStep() {
  int newShell = _model->neighborShell();
  _jumps[_previousNeighborShell][newShell] += 1;
  _previousNeighborShell = newShell;
}

void AnalyzerJumpCount::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug) << "Writing jump count analyzer results:" << std::endl;

  std::ofstream resultsFile;
  resultsFile.open(resultFile());
  resultsFile << "jump count" << std::endl;
  for (size_t start = 0; start < 10; ++start) {
    for (size_t end = 0; end < 10; ++end) {
      if (_jumps[start][end] > 0) {
        resultsFile << start << "-" << end << " " << _jumps[start][end]
                    << std::endl;
      }
    }
  }
  resultsFile.close();
}
