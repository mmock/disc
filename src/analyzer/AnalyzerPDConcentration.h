///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AnalyzerBase.h"

class IrradiationModel;

class AnalyzerPDConcentration : public AnalyzerBase<IrradiationModel> {
 public:
  /// Constructor
  AnalyzerPDConcentration(IrradiationModel* model, libconfig::Setting& cfg);

  /// Parse config
  void parse();

  /// Saves the concentration of the last time interval
  void saveInterval();

  /// Register step
  void registerStep() override;

  /// Write Results to file
  void writeResults() const override;

 private:
  /// Adds up the time 'index' vacancies were present in the system
  std::vector<double> _vacancyTime;

  /// Adds up the time 'index' interstitials were present in the system
  std::vector<double> _interstitialTime;

  /// Number of vacancíes at the end of last step
  size_t _vacancyCount;

  /// Number of interstitials at the end of last step
  size_t _interstitialCount;

  /// Steps between concentration measurements
  double _measurementInterval;

  /// Determines if time axis is on a logarithmic scale
  bool _logarithmicScale;

  /// Next measurement step
  double _nextMeasurement;

  /// Saves the times we did a measurement
  std::vector<double> _measurementTimes;

  /// Vacancy concentration
  std::vector<double> _vacancyConcentration;

  /// Interstitial concentration
  std::vector<double> _interstitialConcentration;
};
