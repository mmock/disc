///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerSphericConcentration.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../events/JumpEventIrradiation.h"
#include "../models/IrradiationModel.h"

AnalyzerSphericConcentration::AnalyzerSphericConcentration(
    IrradiationModel* model, libconfig::Setting& cfg)
    : AnalyzerBase<IrradiationModel>(cfg, model) {
  parse();

  _pointDefectCount = _model->eventObjects().size();
  _measurementTime = 0;
  _shellLimit.resize(_shellCount, 0);
  _shellLocation.resize(_shellCount, 0);
  _siteCount.resize(_shellCount, 0);
  _vacancyCount.resize(_shellCount, 0);
  _vacancyTime.resize(_shellCount, 0);
  _interstitialCount.resize(_shellCount, 0);
  _interstitialTime.resize(_shellCount, 0);

  // Create Shells
  double maxDistance = _model->lattice()->cellSize()[0] * sqrt(3) / 2;
  double binSize = maxDistance / _shellCount;
  for (size_t i = 0; i < _shellCount; ++i) {
    _shellLimit[i] = (i + 1) * binSize;
    _shellLocation[i] = binSize * (2 * i + 1) / 2;
  }

  // Count sites in shell
  for (auto const& site : _model->lattice()->sites()) {
    _siteCount[getShell(site)]++;
  }

  for (auto count : _siteCount) {
    if (!count) {
      std::cerr << "Error: Shell is empty. Reduce number of shells."
                << std::endl;
      exit(EXIT_FAILURE);
    }
  }
}

void AnalyzerSphericConcentration::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing spheric point defect concentration analyzer:"
                    << std::endl;
  AnalyzerBase::parse();

  _waitTime = parseDouble(config(), "WaitTime");
  MsgLogger(normal) << "WaitTime: " << _waitTime << std::endl;

  _shellCount = parseInt(config(), "ShellNumber");
  MsgLogger(normal) << "ShellNumber: " << _shellCount << std::endl;

  _center = parseVector(config(), "Center");
  MsgLogger(normal) << "Center: " << printVector(_center) << std::endl;
}

size_t AnalyzerSphericConcentration::getShell(const Site& site) {
  double distance = _model->lattice()->calculateDistance(site.coord(), _center);
  auto it = std::lower_bound(_shellLimit.begin(), _shellLimit.end(), distance);
  return it - _shellLimit.begin();

  std::cerr << "Error: getShell() failed. Distance (" << distance
            << ") was bigger than the biggest limit (" << _shellLimit.back()
            << ")" << std::endl;
  exit(EXIT_FAILURE);
}

void AnalyzerSphericConcentration::rebuildCounts() {
  std::fill(_vacancyCount.begin(), _vacancyCount.end(), 0);
  std::fill(_interstitialCount.begin(), _interstitialCount.end(), 0);
  for (auto const& object : _model->eventObjects()) {
    if (object->type() == Vacancy) {
      ++_vacancyCount[getShell(object->site())];
    } else if (object->type() == Interstitial) {
      ++_interstitialCount[getShell(object->site())];
    } else {
      continue;
    }
  }
  _pointDefectCount = _model->eventObjects().size();
}

void AnalyzerSphericConcentration::updateCounts(Event* event) {
  auto jump = static_cast<JumpEventIrradiation*>(event);
  size_t oldShell = getShell(jump->start());
  size_t newShell = getShell(jump->target());
  if (oldShell == newShell) return;

  if (jump->target().object()->type() == Interstitial) {
    --_interstitialCount[oldShell];
    ++_interstitialCount[newShell];
  } else {
    --_vacancyCount[oldShell];
    ++_vacancyCount[newShell];
  }
}

void AnalyzerSphericConcentration::registerStep() {
  if (_model->algorithm()->time() < _waitTime) return;

  // Check if a simple jump without defect removal happened
  Event* event = _model->algorithm()->lastEvent();
  if (event->type() == Event::Jump &&
      _pointDefectCount == _model->eventObjects().size()) {
    updateCounts(event);
  } else {
    rebuildCounts();
  }

  double stepDuration = _model->algorithm()->stepDuration();
  _measurementTime += stepDuration;

  // Add up the the total time
  for (size_t i = 0; i < _shellCount; ++i) {
    _vacancyTime[i] += _vacancyCount[i] * stepDuration;
    _interstitialTime[i] += _interstitialCount[i] * stepDuration;
  }
}

void AnalyzerSphericConcentration::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug)
      << "Writing spheric point defect concentration analyzer results to: '"
      << resultFile() << "'" << std::endl;

  if (_measurementTime == 0) {
    MsgLogger(normal) << "Warning: Spheric concentration analyzer hasn't "
                         "started measuring yet!"
                      << std::endl;
  }

  std::ofstream resultsFile;
  resultsFile.open(resultFile());
  resultsFile << "Shell Distance C(Vac) C(Int) N(sites)" << std::endl;
  for (size_t j = 0; j < _shellCount; ++j) {
    resultsFile << j << " " << _shellLocation[j] << " "
                << _vacancyTime[j] / _measurementTime / _siteCount[j] << " "
                << _interstitialTime[j] / _measurementTime / _siteCount[j]
                << " " << _siteCount[j] << std::endl;
  }
  resultsFile.close();
}
