///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerVacancyPosition.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../models/DiffusionModelSolute.h"

AnalyzerVacancyPosition::AnalyzerVacancyPosition(DiffusionModelSolute* model,
                                                 libconfig::Setting& cfg)
    : AnalyzerBase<DiffusionModelSolute>(cfg, model) {
  parse();
  _positionCount = std::vector<int>(10, 0);
  _positionTime = std::vector<double>(10, 0.0);
  _previousNeighborShell = _model->neighborShell();
}

void AnalyzerVacancyPosition::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing vacancy position analyzer:" << std::endl;
  AnalyzerBase::parse();
}

void AnalyzerVacancyPosition::registerStep() {
  _positionCount[_previousNeighborShell] += 1;
  _positionTime[_previousNeighborShell] += _model->algorithm()->stepDuration();
  _previousNeighborShell = _model->neighborShell();
}

void AnalyzerVacancyPosition::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug) << "Writing vacancy position analyzer results:" << std::endl;

  std::ofstream resultsFile;
  resultsFile.open(resultFile());
  resultsFile << "Position Count Time" << std::endl;
  for (size_t j = 0; j < _positionCount.size(); ++j) {
    resultsFile << j << " " << _positionCount[j] << " " << _positionTime[j]
                << std::endl;
  }
  resultsFile.close();
}
