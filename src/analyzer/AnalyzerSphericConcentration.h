///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AnalyzerBase.h"

class Site;
class IrradiationModel;
class Event;

class AnalyzerSphericConcentration : public AnalyzerBase<IrradiationModel> {
 public:
  /// Constructor
  AnalyzerSphericConcentration(IrradiationModel* model,
                               libconfig::Setting& cfg);

  void parse();
  void registerStep() override;
  void writeResults() const override;

 private:
  size_t getShell(const Site& site);
  void rebuildCounts();
  void updateCounts(Event* event);

  double _waitTime;
  double _measurementTime;
  Vector3 _center;
  size_t _shellCount;
  size_t _pointDefectCount;

  std::vector<size_t> _siteCount;
  std::vector<double> _shellLimit;
  std::vector<double> _shellLocation;

  std::vector<size_t> _vacancyCount;
  std::vector<double> _vacancyTime;

  std::vector<size_t> _interstitialCount;
  std::vector<double> _interstitialTime;
};
