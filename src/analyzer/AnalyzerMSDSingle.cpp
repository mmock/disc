///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerMSDSingle.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../models/DiffusionModel.h"

/// Constructor
AnalyzerMSDSingle::AnalyzerMSDSingle(DiffusionModel* model,
                                     libconfig::Setting& cfg)
    : AnalyzerMSD<DiffusionModel>(cfg, model) {
  _oldPosition = _model->defectCoordinate();
  _crossings = {0, 0, 0};
  _startPositions.resize(_nParallel);
}

/// Calculate real position including jumps through the boundary
void AnalyzerMSDSingle::unwrapPositions() {
  _newPosition = _model->defectCoordinate();
  // check if the jump is through the boundary
  for (size_t i = 0; i < 3; i++) {
    double delta = _newPosition[i] - _oldPosition[i];
    if (delta >= _model->lattice()->cellSize()[i] * 0.5) {
      _crossings[i] -= 1;
    } else if (delta <= -_model->lattice()->cellSize()[i] * 0.5) {
      _crossings[i] += 1;
    }
    // calculate total position
    _unwrappedPosition[i] =
        _newPosition[i] + (_crossings[i] * _model->lattice()->cellSize()[i]);
  }
  _oldPosition = _newPosition;
}

/// Collect the starting positions
void AnalyzerMSDSingle::initialize() {
  double time = _model->algorithm()->time();

  if (time > _parallelStartTimes[_nextStartIndex]) {
    _startPositions[_nextStartIndex].resize(_nTau);
    for (size_t j = 0; j < _nTau; j++) {
      _startPositions[_nextStartIndex][j] = _unwrappedPosition;
    }
    _nextStartIndex += 1;
  }

  if (_nextStartIndex >= _nParallel) {
    if (time > 2 * _tau[0]) {
      std::cerr
          << "MSD Initialization failed. Last start position was set after\n"
          << " measurement should already have started. Decrease number of\n"
          << " parallel measurements or increase MinTauSteps. " << time
          << std::endl;
      exit(EXIT_FAILURE);
    }
    _state = measuring;
    MsgLogger(debug) << "MSD: Initialization phase over" << std::endl;
  }
}

/// measure MSD
void AnalyzerMSDSingle::measure() {
  double time = _model->algorithm()->time();
  double sqd;
  double delta;

  // check if a stopTime is reached...
  for (size_t i = 0; i < _nParallel; ++i) {
    for (size_t j = 0; j < _nTau; ++j) {
      if (time > _stopTimes[i][j]) {
        // calculate sqared Displacement
        sqd = 0;
        for (size_t k = 0; k < 3; k++) {
          delta = _unwrappedPosition[k] - _startPositions[i][j][k];
          sqd += delta * delta;
        }
        _sqdValues[j] += sqd;
        _sqdCounts[j] += 1;

        // update startPos and stopTimes
        _stopTimes[i][j] = _stopTimes[i][j] + _tau[j];
        _startPositions[i][j] = _unwrappedPosition;
      }
    }
  }
}
