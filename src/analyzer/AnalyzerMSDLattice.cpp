///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerMSDLattice.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../events/JumpEvent.h"
#include "../models/Model.h"

/// Constructor
AnalyzerMSDLattice::AnalyzerMSDLattice(Model* model, libconfig::Setting& cfg)
    : AnalyzerMSD<Model>(cfg, model) {
  // save start positions
  size_t atomCount = _model->lattice()->numSites();
  _unwrappedPosition.resize(atomCount);
  _crossings.resize(atomCount, {0, 0, 0});
  _startPositions.resize(atomCount,
                         std::vector<std::vector<Vector3>>(
                             _nParallel, std::vector<Vector3>(_nTau)));
  for (Site site : _model->lattice()->sites()) {
    _unwrappedPosition[site.atomIndex()] = site.coord();
  }
}

// Updates the unwrapped positions
void AnalyzerMSDLattice::unwrapPositions() {
  Event* event = _model->algorithm()->lastEvent();
  JumpEvent* jump = dynamic_cast<JumpEvent*>(event);
  if (!jump) return;

  size_t atomIndex = jump->start().atomIndex();
  assert(jump->start().object() == nullptr);
  for (size_t i = 0; i < 3; i++) {
    // Lattice atom moves from target to start
    double delta = jump->start().coord()[i] - jump->target().coord()[i];
    // Check if jump is through the periodic boundary
    if (delta >= _model->lattice()->cellSize()[i] * 0.5) {
      _crossings[atomIndex][i] -= 1;
    } else if (delta <= -_model->lattice()->cellSize()[i] * 0.5) {
      _crossings[atomIndex][i] += 1;
    }
    _unwrappedPosition[atomIndex][i] =
        jump->start().coord()[i] +
        (_crossings[atomIndex][i] * _model->lattice()->cellSize()[i]);
  }
}

/// Collect the starting positions
void AnalyzerMSDLattice::initialize() {
  double time = _model->algorithm()->time();

  if (time > _parallelStartTimes[_nextStartIndex]) {
    for (Site site : _model->lattice()->sites()) {
      if (site.object() == nullptr) {
        size_t atomIndex = site.atomIndex();
        for (size_t j = 0; j < _nTau; j++) {
          _startPositions[atomIndex][_nextStartIndex][j] =
              _unwrappedPosition[atomIndex];
        }
      }
    }
    _nextStartIndex += 1;
  }

  if (_nextStartIndex >= _nParallel) {
    if (time > 2 * _tau[0]) {
      std::cerr
          << "MSD Initialization failed. Last start position was set after\n"
          << "measurement should already have started. Decrease number of\n"
          << "parallel measurements or increase MinTauSteps. " << std::endl;
      exit(EXIT_FAILURE);
    }
    _state = measuring;
    MsgLogger(debug) << "MSD: Initialization phase over" << std::endl;
  }
}

/// measure MSD
void AnalyzerMSDLattice::measure() {
  double time = _model->algorithm()->time();
  double sqd;
  double delta;

  // check if a stopTime is reached...
  for (size_t i = 0; i < _nParallel; ++i) {
    for (size_t j = 0; j < _nTau; ++j) {
      if (time > _stopTimes[i][j]) {
        for (Site site : _model->lattice()->sites()) {
          if (site.object() == nullptr) {
            // calculate sqared Displacement of all Lattice atoms
            size_t atomIndex = site.atomIndex();
            sqd = 0;
            for (size_t k = 0; k < 3; k++) {
              delta = _unwrappedPosition[atomIndex][k] -
                      _startPositions[atomIndex][i][j][k];
              sqd += delta * delta;
            }
            _sqdValues[j] += sqd;
            _sqdCounts[j] += 1;
            // update startPos
            _startPositions[atomIndex][i][j] = _unwrappedPosition[atomIndex];
          }
        }
        // update stopTimes
        _stopTimes[i][j] = _stopTimes[i][j] + _tau[j];
      }
    }
  }
}
