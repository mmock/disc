///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AnalyzerBase.h"

template <class Model>
class AnalyzerMSD : public AnalyzerBase<Model> {
 public:
  AnalyzerMSD(libconfig::Setting& cfg, Model* model)
      : AnalyzerBase<Model>(cfg, model) {
    parse();
    _state = waiting;
    _nextStartIndex = 0;
    _stopTimes.resize(_nParallel);
    _sqdValues = std::vector<double>(_nTau, 0);
    _sqdCounts = std::vector<size_t>(_nTau, 0);
  }

  const std::vector<double>& tau() const { return _tau; }
  const std::vector<double>& sqdValues() const { return _sqdValues; }
  const std::vector<size_t>& sqdCounts() const { return _sqdCounts; }

  void registerStep() override;
  void writeResults() const override;

 protected:
  enum AnalyzerState { waiting, initializing, measuring };
  AnalyzerState _state;

  void parse();
  void wait();
  virtual void initialize() = 0;
  virtual void measure() = 0;
  virtual void unwrapPositions() = 0;

  size_t _nTau;
  size_t _nParallel;
  size_t _nextStartIndex;

  double _tauFactor;
  int64_t _minTauSteps;

  std::vector<double> _parallelStartTimes;
  std::vector<std::vector<double>> _stopTimes;

  std::vector<double> _tau;
  std::vector<double> _sqdValues;
  std::vector<size_t> _sqdCounts;
};

template <class Model>
void AnalyzerMSD<Model>::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing MSD analyzer:" << std::endl;
  AnalyzerBase<Model>::parse();

  _minTauSteps = parseInt(this->config(), "MinTauSteps");
  MsgLogger(normal) << "MinTauSteps: " << _minTauSteps << std::endl;

  _tauFactor = parseDouble(this->config(), "TauFactor");
  MsgLogger(normal) << "TauFactor: " << _tauFactor << std::endl;

  _nTau = parseInt(this->config(), "TauNumber");
  MsgLogger(normal) << "TauNumber: " << _nTau << std::endl;

  _nParallel = parseInt(this->config(), "ParallelMeasurements");
  MsgLogger(normal) << "ParallelMeasurements: " << _nParallel << std::endl;
}

template <class Model>
void AnalyzerMSD<Model>::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug) << "Writing MSD analyzer results:" << std::endl;

  if (_state != measuring) {
    MsgLogger(normal) << "Warning: MSD Analyzer hasn't reached measurement \n"
                      << "         phase yet. Skipping writeResults!"
                      << std::endl;
    return;
  }

  std::ofstream resultsFile;
  resultsFile.open(this->resultFile());
  resultsFile << "tau msd count" << std::endl;
  for (size_t j = 0; j < _nTau; ++j) {
    resultsFile << _tau[j] << " " << _sqdValues[j] / _sqdCounts[j] << " "
                << _sqdCounts[j] << std::endl;
  }
  resultsFile.close();
}

template <class Model>
void AnalyzerMSD<Model>::registerStep() {
  // Get time and position
  unwrapPositions();
  if (_state == waiting) {
    wait();
  } else if (_state == initializing) {
    initialize();
  } else {
    measure();
  }
}

template <class Model>
void AnalyzerMSD<Model>::wait() {
  if (this->_model->algorithm()->step() == _minTauSteps) {
    double time = this->_model->algorithm()->time();
    // Calculate time intervals
    for (size_t i = 0; i < _nTau; i++) {
      _tau.push_back(time + i * (_tauFactor - 1) * time / _nTau);
    }
    // Calculate start and stop times for the parallel measurements
    for (size_t i = 0; i < _nParallel; i++) {
      double startTime = time + i * time / _nParallel;
      _parallelStartTimes.push_back(startTime);
      for (double tau : _tau) {
        _stopTimes[i].push_back(tau);
      }
    }
    // Waiting Phase is over
    _state = initializing;
    MsgLogger(debug) << "MSD: Waiting phase over, duration was " << time
                     << std::endl;
  }
}