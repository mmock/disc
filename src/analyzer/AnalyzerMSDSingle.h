///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "AnalyzerMSD.h"

class DiffusionModel;

class AnalyzerMSDSingle : public AnalyzerMSD<DiffusionModel> {
 public:
  AnalyzerMSDSingle(DiffusionModel* model, libconfig::Setting& cfg);

 private:
  void initialize();
  void measure();
  void unwrapPositions();

  std::array<int, 3> _crossings;
  Vector3 _unwrappedPosition;
  Vector3 _oldPosition;
  Vector3 _newPosition;
  std::vector<std::vector<Vector3>> _startPositions;
};
