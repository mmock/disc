///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "AnalyzerPDConcentration.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../models/IrradiationModel.h"

AnalyzerPDConcentration::AnalyzerPDConcentration(IrradiationModel* model,
                                                 libconfig::Setting& cfg)
    : AnalyzerBase<IrradiationModel>(cfg, model) {
  parse();
  _vacancyCount = _model->vacancyCount();
  _interstitialCount = _model->interstitialCount();
  _nextMeasurement = _measurementInterval;
}

void AnalyzerPDConcentration::parse() {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing point defect concentration analyzer:"
                    << std::endl;
  AnalyzerBase::parse();

  _measurementInterval = parseOptionalDouble(config(), "Interval", 0);
  _logarithmicScale = parseOptionalBool(config(), "LogarithmicScale", false);
  MsgLogger(normal) << "Interval: " << _measurementInterval << std::endl;
}

void AnalyzerPDConcentration::saveInterval() {
  // Save the concentrations in the last interval
  size_t numSites = _model->lattice()->numSites();

  // Add up number of vacancies and the corresponding time
  double totalTime = 0;
  double vacancyConcentration = 0;
  for (size_t i = 0; i < _vacancyTime.size(); i++) {
    vacancyConcentration += i * _vacancyTime[i];
    totalTime += _vacancyTime[i];
  }
  _vacancyConcentration.push_back(vacancyConcentration /
                                  (totalTime * numSites));

  // Do the same thing for interstitials
  totalTime = 0;
  double interstitialConcentration = 0;
  for (size_t i = 0; i < _interstitialTime.size(); i++) {
    interstitialConcentration += i * _interstitialTime[i];
    totalTime += _interstitialTime[i];
  }
  _interstitialConcentration.push_back(interstitialConcentration /
                                       (totalTime * numSites));

  // Calculate the time the measurement interval lies around
  _measurementTimes.push_back(_model->algorithm()->time() - totalTime / 2);

  // Clear the vectors and get the time when the next interval is over
  std::fill(_vacancyTime.begin(), _vacancyTime.end(), 0);
  std::fill(_interstitialTime.begin(), _interstitialTime.end(), 0);
  if (_logarithmicScale) {
    _nextMeasurement *= 2;
  } else {
    _nextMeasurement += _measurementInterval;
  }
}

void AnalyzerPDConcentration::registerStep() {
  double stepDuration = _model->algorithm()->stepDuration();

  // Check if the current number of vacancies already occured
  if (_vacancyCount >= _vacancyTime.size()) {
    _vacancyTime.resize(_vacancyCount + 1, 0);
  }

  // Check if the current number of interstitials already occured
  if (_interstitialCount >= _interstitialTime.size()) {
    _interstitialTime.resize(_interstitialCount + 1, 0);
  }
  _vacancyTime[_vacancyCount] += stepDuration;
  _interstitialTime[_interstitialCount] += stepDuration;

  // Update Number of vacancies
  _vacancyCount = _model->vacancyCount();
  _interstitialCount = _model->interstitialCount();

  if (_nextMeasurement && _model->algorithm()->step() > _nextMeasurement) {
    saveInterval();
  }
}

void AnalyzerPDConcentration::writeResults() const {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug)
      << "Writing point defect concentration analyzer results to: '"
      << resultFile() << "'" << std::endl;
  std::ofstream resultsFile;
  resultsFile.open(resultFile());
  resultsFile << "Time C(vac) C(int)" << std::endl;
  for (size_t j = 0; j < _vacancyConcentration.size(); ++j) {
    resultsFile << _measurementTimes[j] << " " << _vacancyConcentration[j]
                << " " << _interstitialConcentration[j] << std::endl;
  }
  resultsFile.close();
}
