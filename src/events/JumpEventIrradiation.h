///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../models/IrradiationModel.h"
#include "../objects/EventObject.h"
#include "Event.h"

class Site;

class JumpEventIrradiation : public Event {
 public:
  JumpEventIrradiation(IrradiationModel* model, EventObject* object,
                       Site* origin, Site* target, double rate)
      : Event(rate, Jump),
        _model(model),
        _object(object),
        _origin(origin),
        _target(target){};

  const Site& target() const { return *_target; }
  const Site& start() const { return *_origin; }

  void execute() override {
    _origin->object()->move(_target);

    flagNeighbors(*_origin);
    flagNeighbors(*_target);

    // Update neighbor list
    if (_object->type() == Interstitial) {
      _model->interstitialNeighborList()->updateCell(_object, _origin->coord());
    } else if (_object->type() == Vacancy) {
      _model->vacancyNeighborList()->updateCell(_object, _origin->coord());
    } else {
      throw std::runtime_error("No Neighborlist for this defect type!");
    }

    _model->checkRecombination(_object);
  }

 private:
  IrradiationModel* _model;

  EventObject* _object;
  Site* _origin;
  Site* _target;
};
