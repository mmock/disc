///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../lattice/Site.h"
#include "../models/IrradiationModel.h"
#include "../objects/EventObject.h"
#include "Event.h"

class SpawnEvent : public Event {
 public:
  SpawnEvent(IrradiationModel* model, double rate, ObjectType type)
      : Event(rate, Irradiation), _model(model), _type(type){};

  void execute() override {
    // find a random site that is not occupied
    size_t r1;
    size_t numSites = _model->lattice()->numSites();
    Site* s1;
    while (true) {
      r1 = _model->algorithm()->randomIndex(numSites);
      s1 = &_model->lattice()->sites()[r1];
      if (s1->type() == LatticeAtom) {
        break;
      }
    }

    if (_type == Vacancy) {
      _model->createVacancy(*s1);
    } else {
      _model->createInterstitial(*s1);
    }
  }

 private:
  IrradiationModel* _model;
  ObjectType _type;
};
