///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../objects/EventObject.h"
#include "Event.h"

class Site;

/*
   This Event is a combination of two jumps that only happens in the
   DiffusionModelYttrium. The vacancy jumps to a neighbor on the far side of the
   yttrium atom and the yttrium atom moves to the former position of the
   vacancy.
*/
class JumpSwitchEvent : public Event {
 public:
  JumpSwitchEvent(Site* origin, BasicObject* yttrium, Site* target, double rate)
      : Event(rate, JumpSwitch), _origin(origin), _target(target), _yttrium(yttrium){};

  void execute() override {
    _origin->object()->move(_target);
    _yttrium->move(_origin);
  }

 protected:
  Site* _origin;
  Site* _target;
  BasicObject* _yttrium;
};
