///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

class Event {
   public:

  enum EventType {
    Jump,
    JumpSwitch,
    Irradiation,
    Destruction
};
  Event(double rate, EventType type) : _rate(rate), _type(type) {};

  double getRate() const { return _rate; }
  EventType type() const { return _type; }

  virtual void execute() = 0;

 private:
  double _rate;
  EventType _type;
};
