///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../lattice/Site.h"
#include "../models/IrradiationModel.h"
#include "../objects/EventObject.h"
#include "Event.h"

const size_t maximumNumberOfAttempts = 1e6;

class IrradiationEvent : public Event {
 public:
  IrradiationEvent(IrradiationModel* model, double rate)
      : Event(rate, Irradiation), _model(model){};

  void execute() override {
    // find a random site that is not occupied
    size_t r1, r2;
    size_t numSites = _model->lattice()->numSites();
    Site* s1;
    Site* s2;
    for (size_t i = 0; true; ++i) {
      r1 = _model->algorithm()->randomIndex(numSites);
      s1 = &_model->lattice()->sites()[r1];
      if (s1->type() == LatticeAtom) {
        break;
      }

      if (i == maximumNumberOfAttempts) {
        throw std::runtime_error(
            "Could not find suitable sites the creation of an "
            "interstitial-vacancy pair.");
      }
    }
    for (size_t i = 0; true; ++i) {
      r2 = _model->algorithm()->randomIndex(numSites);
      s2 = &_model->lattice()->sites()[r2];
      double d = _model->lattice()->calculateDistance(s1->coord(), s2->coord());
      if (s2->type() == LatticeAtom && r1 != r2 &&
          d > _model->frenkelPairRadius()) {
        break;
      }

      if (i == maximumNumberOfAttempts) {
        throw std::runtime_error(
            "Could not find suitable sites the creation of an "
            "interstitial-vacancy pair.");
      }
    }
    _model->createInterstitial(*s1);
    _model->createVacancy(*s2);

    if (s1->object()) {
      _model->checkRecombination(static_cast<EventObject*>(s1->object()));
    }
    if (s2->object()) {
      _model->checkRecombination(static_cast<EventObject*>(s2->object()));
    }
  }

 private:
  IrradiationModel* _model;
};
