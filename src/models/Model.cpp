///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Model.h"
#include "../lattice/BCClattice.h"
#include "../lattice/FCClattice.h"

void Model::getConfig(const char *configFile) {
  // Parse input file
  try {
    _config.readFile(configFile);
  } catch (const libconfig::FileIOException &fioex) {
    std::cerr << "I/O error while reading file '" << configFile << "'"
              << std::endl;
    exit(EXIT_FAILURE);
  } catch (const libconfig::ParseException &pex) {
    std::cerr << "Parse error at " << pex.getFile() << ":" << pex.getLine()
              << " - " << pex.getError() << std::endl;
    exit(EXIT_FAILURE);
  }
  _config.setAutoConvert(true);
}

void Model::parse(const char *configFile) {
  getConfig(configFile);
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing Model:" << std::endl;

  _algo = std::make_unique<KMC>(parseSetting(config(), "Algorithm"));

  if (config().exists("Dump")) {
    libconfig::Setting &dumpSetting = parseSetting(config(), "Dump");
    _dump = std::make_unique<Dump>(dumpSetting);
  }

  if (config().exists("Thermo")) {
    libconfig::Setting &thermoSetting = parseSetting(config(), "Thermo");
    _thermo = std::make_unique<Thermo>(thermoSetting);
  }

  // Output Frequency
  _outputFrequency = parseInt(config(), "OutputFrequency");
  MsgLogger(normal) << "OutputFrequency: " << _outputFrequency << std::endl;
  // Temperature
  setTemperature(parseDouble(config(), "Temperature"));
  MsgLogger(normal) << "Temperature: " << _temperature << std::endl;
  // parse attemptFrequency
  _attemptFrequency = parseDouble(config(), "AttemptFrequency");
  MsgLogger(normal) << "Parsing attemptFrequency: " << _attemptFrequency
                    << std::endl;

  // Parse Lattice
  createLattice(parseSetting(config(), "Lattice"));
}

void Model::createLattice(const libconfig::Setting &latticeSetting) {
  Vector3 size = parseVector(latticeSetting, "SystemSize");
  if (parseString(latticeSetting, "Type") == "BCC") {
    _lattice = std::make_unique<BCCLattice>(size);
  } else if (parseString(latticeSetting, "Type") == "FCC") {
    _lattice = std::make_unique<FCCLattice>(size);
  }
}

void Model::writeResults() const {
  // Write Dump
  if (dump()) {
    dump()->writeStep(*lattice(), algorithm()->step());
  }

  // Write Thermo
  if (thermo()) {
    thermo()->writeStep(stepStatus(), algorithm()->step());
  }

  // Comandline Output
  if (_outputFrequency != 0 && _algo->step() % _outputFrequency == 0) {
    MsgLogger(normal) << stepStatus();
  }
}

void Model::takeStep() {
  // Make sure that events are up to date
  updateEvents();
  _algo->takeStep(_events);
  updateModel();

  // Do the analysis
  for (auto &ana : analyzer()) {
    ana->registerStep();
  }
  writeResults();
}

/// Calculates the rate from the energy
double Model::calculateRate(double activationEnergy) const {
  return _attemptFrequency * exp(activationEnergy * _prefactor);
}
