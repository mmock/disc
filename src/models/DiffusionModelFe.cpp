///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DiffusionModelFe.h"
#include "../analyzer/AnalyzerMSDSingle.h"
#include "../objects/VacancyObject.h"

/// Parses config
void DiffusionModelFe::parse(const char* configFile) {
  Model::parse(configFile);
  MsgLogger(debug) << "Parsing diffusion model Fe:" << std::endl;

  // parse migration barrier
  _migrationBarrier = parseDouble(config(), "MigrationBarrier");
  MsgLogger(normal) << "Parsing MigrationBarrier: " << _migrationBarrier
                    << std::endl;

  // calculate vacancy jump rate
  _vacancyJumpRate = calculateRate(_migrationBarrier);
  MsgLogger(normal) << "Calculated VacancyJumpRate: " << _vacancyJumpRate
                    << std::endl;

  // parse vacancy index and create vacancy
  int i = parseInt(config(), "VacancyIndex");
  MsgLogger(normal) << "VacancyIndex: " << i << std::endl;
  if (lattice()->sites()[i].type() == LatticeAtom) {
    // Vacancy<DiffusionModelFe> vac = Vacancy(this, &lattice()->sites()[i]);
    _vacancy = std::make_unique<VacancyObject<DiffusionModelFe>>(
        this, &lattice()->sites()[i]);
  } else {
    std::cerr << "Error: Vacancy site '" << i << "' is already occupied with '"
              << lattice()->sites()[i].type() << "'." << std::endl;
    exit(EXIT_FAILURE);
  }

  if (config().exists("Analyzer")) {
    libconfig::Setting& analyzerList = config()["Analyzer"];
    for (int i = 0; i < analyzerList.getLength(); ++i) {
      libconfig::Setting& analyzer = analyzerList[i];
      if (parseString(analyzer, "Name") == "MSD") {
        _analyzer.push_back(
            std::make_unique<AnalyzerMSDSingle>(this, analyzer));
      } else {
        std::cerr << "Error: Unknown analyzer '"
                  << parseString(analyzer, "Name") << "'" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }
}

/// Tell the vacancy to update the jumps and reset the event list
void DiffusionModelFe::updateEvents() {
  _vacancy->createEventList();
  events().clear();
  for (auto& event : _vacancy->events()) {
    events().push_back(event.get());
  }
}

/// Get status for commandline/thermo
std::string DiffusionModelFe::stepStatus() const {
  std::ostringstream buffer;
  if (algorithm()->step() == 0) {
    buffer << "   Step Time x y z" << std::endl;
  }
  buffer << std::setprecision(15) << "   " << algorithm()->step() << " "
         << algorithm()->time() << " " << _vacancy->position()[0] << " "
         << _vacancy->position()[1] << " " << _vacancy->position()[2]
         << std::endl;
  return buffer.str();
}