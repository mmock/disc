///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "DiffusionModelSolute.h"

/**
 * The Diffusion model for the special case of yttrium with nearest neighbor
 * interactions. Introduces a split position is yttrium and vacancy are next to
 * each other.
 */
class DiffusionModelYttrium : public DiffusionModelSolute {
 public:
  DiffusionModelYttrium() : DiffusionModelSolute(){};

  void updateModel() override;
  const Vector3& defectCoordinate() const override { return _yttriumPosition; }

  double jumpSwitchRate(const Site& vacancySite, const Site& targetSite) const;
  void createVacancy(int siteIndex) override;

 private:
  Vector3 _yttriumPosition;
};
