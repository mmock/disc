///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../IO/Dump.h"
#include "../IO/Thermo.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../analyzer/Analyzer.h"
#include "../disc.h"
#include "../events/Event.h"
#include "../lattice/Lattice.h"

class Model {
 public:
  Model(){};
  virtual ~Model() = default;

  /// Public functions
  virtual void parse(const char* configFile);
  virtual void updateEvents() = 0;
  virtual void updateModel() = 0;
  std::vector<Event*>& events() { return _events; }
  void takeStep();
  void writeResults() const;
  double calculateRate(double activationEnergy) const;

  /// Getters
  const libconfig::Setting& config() const { return _config.getRoot(); }
  std::unique_ptr<Lattice> const& lattice() const { return _lattice; }
  std::unique_ptr<KMC> const& algorithm() const { return _algo; }
  std::unique_ptr<Dump> const& dump() const { return _dump; }
  std::unique_ptr<Thermo> const& thermo() const { return _thermo; }
  std::vector<std::unique_ptr<Analyzer>> const& analyzer() { return _analyzer; }
  const double prefactor() const { return _prefactor; }
  const double attemptFrequency() const { return _attemptFrequency; }

  /// Setters
  void setTemperature(double T) {
    _temperature = T;
    _prefactor = -1.0 / (_temperature * _kb);
  }

 protected:
  std::unique_ptr<Lattice> _lattice;
  std::vector<std::unique_ptr<Analyzer>> _analyzer;

 private:
  void getConfig(const char* configFile);
  virtual std::string stepStatus() const = 0;
  void createLattice(const libconfig::Setting& latticeSetting);

  libconfig::Config _config;
  std::unique_ptr<KMC> _algo;
  std::unique_ptr<Dump> _dump;
  std::unique_ptr<Thermo> _thermo;
  std::vector<Event*> _events;

  double _attemptFrequency;
  double _prefactor;
  double _temperature;
  double _kb = 8.6173324e-5;
  int _outputFrequency;
};
