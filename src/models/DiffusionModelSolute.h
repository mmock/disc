///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/JumpEvent.h"
#include "DiffusionModel.h"

class DiffusionModelSolute : public DiffusionModel {
 public:
  DiffusionModelSolute() : DiffusionModel(){};
  virtual ~DiffusionModelSolute() = default;

  BasicObject* solute() const { return _solute.get(); }
  int neighborShell() const { return _neighborShell; }
  const Vector3& defectCoordinate() const override {
    return _solute->position();
  }

  void parse(const char* configFile);
  std::string stepStatus() const override;
  void updateEvents() override;
  virtual void updateModel() override {
    _neighborShell = neighborShell(_vacancy->site(), _solute->site());
  }
  double vacancyJumpRate(Site* start, Site* target) const;
  virtual void createVacancy(int siteIndex);

 protected:
  int neighborShell(const Site& site1, const Site& site2) const;

  std::unique_ptr<BasicObject> _solute;

  int _neighborShell;
  double _rates[10][10];
};
