///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DiffusionModelSolute.h"
#include "../analyzer/AnalyzerJumpCount.h"
#include "../analyzer/AnalyzerMSDSingle.h"
#include "../analyzer/AnalyzerVacancyPosition.h"
#include "../objects/VacancyObject.h"

/// Read config
void DiffusionModelSolute::parse(const char* configFile) {
  Model::parse(configFile);
  MsgLogger(debug) << "Parsing diffusion model Solute:" << std::endl;

  // Make sure the lattice is BCC
  if (parseString(parseSetting(config(), "Lattice"), "Type") != "BCC") {
    std::cerr << "Error: Lattice Type for Solute and Yttrium diffusion has to "
                 "be 'BCC'.\n"
              << "If you want to use it with another lattice type change "
                 "neighborShell() function."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // parse vacancy index and create vacancy
  int vac = parseInt(config(), "VacancyIndex");
  MsgLogger(normal) << "VacancyIndex: " << vac << std::endl;
  createVacancy(vac);

  // parse solute index and create solute atom
  int sol = parseInt(config(), "SoluteIndex");
  MsgLogger(normal) << "SoluteIndex: " << sol << std::endl;
  if (lattice()->sites()[sol].type() == LatticeAtom) {
    _solute = std::make_unique<BasicObject>(&lattice()->sites()[sol], Solute);
  } else {
    std::cerr << "Error: Solute site '" << sol << "' is already occupied."
              << std::endl;
    exit(EXIT_FAILURE);
  }

  // Parse barriers
  libconfig::Setting& barriers = parseSetting(config(), "Barriers");
  double defaultBarrier = parseDouble(barriers, "j00");
  for (int i = 0; i < 10; i++) {
    for (int j = 0; j < 10; j++) {
      std::string barrierName = "j" + std::to_string(i) + std::to_string(j);
      double barrier =
          parseOptionalDouble(barriers, barrierName, defaultBarrier);
      MsgLogger(debug) << barrierName << ": " << barrier << std::endl;
      _rates[i][j] = calculateRate(barrier);
    }
  }

  updateModel();
  // Analyzer creation must be the last step to make sure _neighborShell is
  // already calculated.
  if (config().exists("Analyzer")) {
    libconfig::Setting& analyzerList = config()["Analyzer"];
    for (int i = 0; i < analyzerList.getLength(); ++i) {
      libconfig::Setting& analyzer = analyzerList[i];
      if (parseString(analyzer, "Name") == "VacancyPosition") {
        _analyzer.push_back(
            std::make_unique<AnalyzerVacancyPosition>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "JumpCount") {
        _analyzer.push_back(
            std::make_unique<AnalyzerJumpCount>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "MSD") {
        _analyzer.push_back(
            std::make_unique<AnalyzerMSDSingle>(this, analyzer));
      } else {
        std::cerr << "Error: Unknown analyzer '"
                  << parseString(analyzer, "Name") << "'" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }
}

void DiffusionModelSolute::createVacancy(int siteIndex) {
  if (lattice()->sites()[siteIndex].type() == LatticeAtom) {
    _vacancy = std::make_unique<VacancyObject<DiffusionModelSolute>>(
        this, &lattice()->sites()[siteIndex]);
  } else {
    std::cerr << "Error: Vacancy site '" << siteIndex
              << "' is already occupied." << std::endl;
    exit(EXIT_FAILURE);
  }
}

/// Tell the vacancy to update the jumps and reset the event list
void DiffusionModelSolute::updateEvents() {
  _vacancy->createEventList();
  events().clear();
  for (auto& event : _vacancy->events()) {
    events().push_back(event.get());
  }
}

double DiffusionModelSolute::vacancyJumpRate(Site* start, Site* target) const {
  int finalNeighborShell = neighborShell(*target, _solute->site());
  return _rates[_neighborShell][finalNeighborShell];
}

int DiffusionModelSolute::neighborShell(const Site& site1,
                                        const Site& site2) const {
  double r = lattice()->calculateDistance(site1.coord(), site2.coord());
  if (r < 0.9)
    return 1;  // 1NN: sqrt(3)/2
  else if (r < 1.1)
    return 2;  // 2NN: 1
  else if (r < 1.5)
    return 3;  // 3NN: sqrt(2)
  else if (r < 1.7)
    return 4;  // 4NN: sqrt(11/4)
  else if (r < 1.8)
    return 5;  // 5NN: sqrt(3)
  else if (r < 2.1)
    return 6;  // 6NN: 2
  else if (r < 2.2)
    return 7;  // 7NN: sqrt(19/4)
  else if (r < 2.3)
    return 8;  // 8NN: sqrt(5)
  else if (r < 2.5)
    return 9;  // 9NN: sqrt(6)
  else
    return 0;  // 10NN (sqrt(27/4)) oder noch weiter...
}

/// Get status for commandline/thermo
std::string DiffusionModelSolute::stepStatus() const {
  std::ostringstream buffer;
  if (algorithm()->step() == 0) {
    buffer << "   Step Time Solute Vacancy" << std::endl;
  }
  buffer << std::setprecision(15) << "   " << algorithm()->step() << " "
         << algorithm()->time() << " (" << _solute->position()[0] << ", "
         << _solute->position()[1] << ", " << _solute->position()[2] << ") ("
         << _vacancy->position()[0] << ", " << _vacancy->position()[1] << ", "
         << _vacancy->position()[2] << ")" << std::endl;
  return buffer.str();
}
