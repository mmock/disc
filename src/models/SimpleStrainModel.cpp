///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "SimpleStrainModel.h"
#include "../fields/AnalyticalStrainField.h"
#include "../fields/FEMStrainField.h"

/// Read config
void SimpleStrainModel::parse(const char* configFile) {
  IrradiationModel::parse(configFile);
  MsgLogger(debug) << "Parsing strain model:" << std::endl;

  libconfig::Setting& strain = parseSetting(config(), "Strain");

  // parse dipole tensor
  parseArray(strain, "VacancyDipoleTensorStart", _vacancyDipoleTensorStart);
  MsgLogger(normal) << "VacancyDipoleTensorStart: "
                    << printVector(_vacancyDipoleTensorStart) << std::endl;
  parseArray(strain, "VacancyDipoleTensorSaddle", _vacancyDipoleTensorSaddle);
  MsgLogger(normal) << "VacancyDipoleTensorSaddle: "
                    << printVector(_vacancyDipoleTensorSaddle) << std::endl;
  parseArray(strain, "InterstitialDipoleTensorStart",
             _interstitialDipoleTensorStart);
  MsgLogger(normal) << "InterstitialDipoleTensorStart: "
                    << printVector(_interstitialDipoleTensorStart) << std::endl;
  parseArray(strain, "InterstitialDipoleTensorSaddle",
             _interstitialDipoleTensorSaddle);
  MsgLogger(normal) << "InterstitialDipoleTensorSaddle: "
                    << printVector(_interstitialDipoleTensorSaddle)
                    << std::endl;

  if (strain.exists("FileName")) {
    _strainField = std::make_unique<FEMStrainField>(lattice(), strain);
  } else if (strain.exists("MatrixShearModulus")) {
    _strainField = std::make_unique<AnalyticalStrainField>(lattice(), strain);
  } else {
    std::cerr << "Error: Definition of strain field ambiguous" << std::endl;
    exit(EXIT_FAILURE);
  }

  MsgLogger(normal) << "Calculating strain field at lattice points"
                    << std::endl;
  _strainField->calculateStrainField();
  calculateRates();
}

// Precalculation of all possible rates.
void SimpleStrainModel::calculateRates() {
  Tensor strain;
  for (const auto& site : _lattice->sites()) {
    strain = strainField()->strain(site);

    // Calculate energy change at lattice position
    double vacStartChange =
        energyChange(strain, _vacancyDipoleTensorStart);
    double intStartChange =
        energyChange(strain, _interstitialDipoleTensorStart);

    // Calculate energy change at saddle point
    double vacSaddleChange =
        energyChange(strain, _vacancyDipoleTensorSaddle);
    double intSaddleChange =
        energyChange(strain, _interstitialDipoleTensorSaddle);

    // Calculate modified rate
    double vacRate = calculateRate(_vacancyMigrationBarrier +
                                    vacSaddleChange - vacStartChange);
    double intRate = calculateRate(_interstitialMigrationBarrier +
                                    intSaddleChange - intStartChange);

    _vacancyJumpRates.push_back(vacRate);
    _interstitialJumpRates.push_back(intRate);
  }
}


double SimpleStrainModel::energyChange(const Tensor& strain, const Tensor& dipole) {
  return -(strain.array() * dipole.array()).sum();
}
