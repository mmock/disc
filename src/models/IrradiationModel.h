///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/JumpEvent.h"
#include "../lattice/NeighborList.h"
#include "../objects/EventObject.h"
#include "Model.h"

class IrradiationModel : public Model {
 public:
  IrradiationModel() : Model(){};
  virtual ~IrradiationModel() = default;

  const size_t vacancyCount() const { return _vacancyCount; }
  const size_t interstitialCount() const { return _interstitialCount; }
  const double frenkelPairRadius() const { return _frenkelPairRadius; }
  virtual double vacancyJumpRate(const Site& start, const Site& target) const {
    return _vacancyJumpRate;
  }
  virtual double interstitialJumpRate(const Site& start,
                                      const Site& target) const {
    return _interstitialJumpRate;
  }
  std::vector<std::unique_ptr<EventObject>> const& eventObjects() {
    return _eventObjects;
  }
  std::unique_ptr<NeighborList>& interstitialNeighborList() {
    return _interstitialNeighborList;
  }
  std::unique_ptr<NeighborList>& vacancyNeighborList() {
    return _vacancyNeighborList;
  }

  virtual void parse(const char* configFile);
  void createSphericalPrecipitate(const libconfig::Setting& setting);
  void createCubicPrecipitate(const libconfig::Setting& setting);
  std::string stepStatus() const override;
  void updateEvents() override;
  void updateModel() override;
  void createVacancy(Site& site);
  void createInterstitial(Site& site);
  size_t objectIndex(EventObject* object) const;
  void removePointDefect(size_t index);
  void checkRecombination(EventObject* obj1);

 private:
  void createRadiationSource(double dose, std::string radiationType);

 protected:
  std::vector<std::unique_ptr<EventObject>> _eventObjects;
  std::vector<std::unique_ptr<BasicObject>> _basicObjects;

  std::unique_ptr<NeighborList> _interstitialNeighborList;
  std::unique_ptr<NeighborList> _vacancyNeighborList;

  double _vacancyMigrationBarrier = 0;
  double _interstitialMigrationBarrier = 0;
  double _vacancyJumpRate = 0;
  double _interstitialJumpRate = 0;
  double _electronIrradiationDose = 0;
  double _frenkelPairRadius = 0;
  double _squaredFrenkel = 0;
  size_t _vacancyCount = 0;
  size_t _interstitialCount = 0;
};

// flag surrounding atoms for recalculation
void flagNeighbors(const Site& site);
bool typesMatchForRecombination(const EventObject& obj1,
                                const EventObject& obj2);