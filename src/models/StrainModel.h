///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../fields/StrainField.h"
#include "IrradiationModel.h"

class StrainModel : public IrradiationModel {
 public:
  StrainModel() : IrradiationModel(){};
  virtual ~StrainModel() = default;

  std::unique_ptr<StrainField> const& strainField() const {
    return _strainField;
  }
  double vacancyJumpRate(const Site& start, const Site& target) const override {
    return _vacancyJumpRates[start.siteIndex()][start.neighborIndex(&target)];
  }
  double interstitialJumpRate(const Site& start,
                              const Site& target) const override {
    return _interstitialJumpRates[start.siteIndex()]
                                 [start.neighborIndex(&target)];
  }
  const Tensor& vacancyDipoleTensorStart() const {
    return _vacancyDipoleTensorStart;
  }
  const Tensor& vacancyDipoleTensorSaddle() const {
    return _vacancyDipoleTensorSaddle;
  }
  const Tensor& interstitialDipoleTensorStart() const {
    return _interstitialDipoleTensorStart;
  }
  const Tensor& interstitialDipoleTensorSaddle() const {
    return _interstitialDipoleTensorSaddle;
  }
  void calculateRates();
  virtual void parse(const char* configFile) override;
  double energyChange(const Tensor& strain, const Tensor& dipole);

 private:
  std::unique_ptr<StrainField> _strainField;

  /// Dipole tensor for vacancy jumps
  Tensor _vacancyDipoleTensorStart;
  Tensor _vacancyDipoleTensorSaddle;
  /// Dipole tensor for interstitial jumps
  Tensor _interstitialDipoleTensorStart;
  Tensor _interstitialDipoleTensorSaddle;

  std::vector<std::vector<double>> _vacancyJumpRates;
  std::vector<std::vector<double>> _interstitialJumpRates;
};

double energyChange(const Tensor& train, const Tensor& dipole);
