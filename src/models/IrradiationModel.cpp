///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "IrradiationModel.h"
#include "../analyzer/AnalyzerLatticeConcentration.h"
#include "../analyzer/AnalyzerMSDLattice.h"
#include "../analyzer/AnalyzerMSDcom.h"
#include "../analyzer/AnalyzerPDConcentration.h"
#include "../analyzer/AnalyzerSphericConcentration.h"
#include "../objects/InterstitialObject.h"
#include "../objects/IrradiationObject.h"
#include "../objects/SpawnObject.h"
#include "../objects/VacancyIrradiation.h"

/// Read config
void IrradiationModel::parse(const char* configFile) {
  Model::parse(configFile);
  MsgLogger(debug) << "Parsing strain model:" << std::endl;

  // parse Vacancy Migration
  _vacancyMigrationBarrier = parseDouble(config(), "VacancyMigrationBarrier");
  MsgLogger(normal) << "Parsing VacancyMigrationBarrier: "
                    << _vacancyMigrationBarrier << std::endl;
  _vacancyJumpRate = calculateRate(_vacancyMigrationBarrier);
  MsgLogger(normal) << "Calculated VacancyJumpRate: " << _vacancyJumpRate
                    << std::endl;

  // parse interstitial Migration
  _interstitialMigrationBarrier =
      parseDouble(config(), "InterstitialMigrationBarrier");
  MsgLogger(normal) << "Parsing InterstitialMigrationBarrier: "
                    << _interstitialMigrationBarrier << std::endl;
  _interstitialJumpRate = calculateRate(_interstitialMigrationBarrier);
  MsgLogger(normal) << "Calculated InterstitialJumpRate: "
                    << _interstitialJumpRate << std::endl;

  // parse radius of frenkel pair recombination
  _frenkelPairRadius = parseDouble(config(), "FrenkelPairRadius");
  _squaredFrenkel = _frenkelPairRadius * _frenkelPairRadius;
  MsgLogger(normal) << "FrenkelPairRadius: " << _frenkelPairRadius << std::endl;

  // parse electron irradiation dose
  if (config().exists("IrradiationDose")) {
    double dose = parseDouble(config(), "IrradiationDose");
    std::string radiationType =
        parseOptionalString(config(), "RadiationType", "Irradiation");
    MsgLogger(normal) << "Parsing IrradiationDose: " << dose << std::endl;
    MsgLogger(normal) << "Parsing IrradiationType: " << radiationType
                      << std::endl;
    createRadiationSource(dose, radiationType);
  }

  // parse Vacancies
  std::vector<size_t> vacancyIndices =
      parseOptionalIndexVector(config(), "Vacancies", _lattice->numSites());
  for (int index : vacancyIndices) {
    createVacancy(_lattice->sites()[index]);
  }
  MsgLogger(normal) << "Vacancies: " << printVector(vacancyIndices)
                    << std::endl;

  // parse Interstitials
  std::vector<size_t> interstitialIndices =
      parseOptionalIndexVector(config(), "Interstitials", _lattice->numSites());
  for (int index : interstitialIndices) {
    createInterstitial(_lattice->sites()[index]);
  }
  MsgLogger(normal) << "Interstitials: " << printVector(interstitialIndices)
                    << std::endl;

  // parse precipitates
  if (config().exists("Precipitates")) {
    auto& precipitateList = parseSetting(config(), "Precipitates");
    for (auto& precSet : precipitateList) {
      if (precSet.exists("Radius")) {
        createSphericalPrecipitate(precSet);
      } else if (precSet.exists("Minimum")) {
        createCubicPrecipitate(precSet);
      } else {
        std::cerr
            << "Parsing Error: Precipitate is neither spherical nor cubic."
            << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }

  // Create neighbor lists
  _interstitialNeighborList = std::make_unique<NeighborList>(
      lattice()->cellSize(), _frenkelPairRadius, _eventObjects, Interstitial);
  _vacancyNeighborList = std::make_unique<NeighborList>(
      lattice()->cellSize(), _frenkelPairRadius, _eventObjects, Vacancy);

  updateModel();
  if (config().exists("Analyzer")) {
    libconfig::Setting& analyzerList = config()["Analyzer"];
    for (int i = 0; i < analyzerList.getLength(); ++i) {
      libconfig::Setting& analyzer = analyzerList[i];
      if (parseString(analyzer, "Name") == "MSD-Lattice") {
        _analyzer.push_back(
            std::make_unique<AnalyzerMSDLattice>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "PointDefectConcentration") {
        _analyzer.push_back(
            std::make_unique<AnalyzerPDConcentration>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "SphericConcentration") {
        _analyzer.push_back(
            std::make_unique<AnalyzerSphericConcentration>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "LatticeConcentration") {
        _analyzer.push_back(
            std::make_unique<AnalyzerLatticeConcentration>(this, analyzer));
      } else if (parseString(analyzer, "Name") == "MSD-COM") {
        _analyzer.push_back(std::make_unique<AnalyzerMSDcom>(this, analyzer));
      } else {
        std::cerr << "Error: Unknown analyzer '"
                  << parseString(analyzer, "Name") << "'" << std::endl;
        exit(EXIT_FAILURE);
      }
    }
  }
}

void IrradiationModel::createCubicPrecipitate(
    const libconfig::Setting& setting) {
  Vector3 min = parseVector(setting, "Minimum");
  Vector3 max = parseVector(setting, "Maximum");
  MsgLogger(normal) << "Parsing Precipitate " << std::endl;
  MsgLogger(normal) << "Minimum: " << printVector(min);
  MsgLogger(normal) << ", Maximum: " << printVector(max) << std::endl;
  // create precipitate
  auto precipitate = std::make_unique<BasicObject>(nullptr, Precipitate);
  for (Site& site : _lattice->sites()) {
    bool inside = true;
    for (size_t i = 0; i < 3; ++i) {
      if (site.coord()[i] < min[i] || site.coord()[i] > max[i]) {
        inside = false;
      }
    }

    if (inside) {
      site.setObject(precipitate.get());
    }
  }
  _basicObjects.push_back(std::move(precipitate));
}

void IrradiationModel::createSphericalPrecipitate(
    const libconfig::Setting& setting) {
  Vector3 center = parseVector(setting, "Center");
  double radius = parseDouble(setting, "Radius");
  MsgLogger(normal) << "Parsing Precipitate " << std::endl;
  MsgLogger(normal) << "Radius: " << radius;
  MsgLogger(normal) << ", Center: " << printVector(center) << std::endl;
  // create precipitate
  auto precipitate = std::make_unique<BasicObject>(nullptr, Precipitate);
  for (Site& site : _lattice->sites()) {
    if (lattice()->calculateDistance(site.coord(), center) <= radius + 1e-9) {
      site.setObject(precipitate.get());
    }
  }
  _basicObjects.push_back(std::move(precipitate));
}

/// update the possible Events
void IrradiationModel::updateEvents() {
  // MsgLogger(debug) << "Model is updating the events" << std::endl;
  events().clear();
  for (const auto& object : eventObjects()) {
    if (object->shouldUpdateEvents()) {
      object->createEventList();
    }
    for (const auto& event : object->events()) {
      events().push_back(event.get());
    }
  }
}

/// update the Model
void IrradiationModel::updateModel() {}

/// Get index of object in event object list.
size_t IrradiationModel::objectIndex(EventObject* object) const {
  auto it = std::find_if(
      _eventObjects.begin(), _eventObjects.end(),
      [&](const std::unique_ptr<EventObject>& p) { return p.get() == object; });
  if (it == _eventObjects.end()) {
    throw std::runtime_error("Unable to find event object in _eventObjects.");
  }
  return std::distance(_eventObjects.begin(), it);
}

/// Check if there is another object for recombination nearby.
void IrradiationModel::checkRecombination(EventObject* obj1) {
  if (obj1->type() == Interstitial) {
    for (const auto& obj2 :
         _vacancyNeighborList->getNeighbors(obj1->position())) {
      double d = lattice()->calculateSquaredDistance(obj1->site().coord(),
                                                     obj2->site().coord());
      if (d < _squaredFrenkel) {
        size_t index1 = objectIndex(obj1);
        size_t index2 = objectIndex(obj2);
        if (index1 > index2) {
          removePointDefect(index1);
          removePointDefect(index2);
        } else {
          removePointDefect(index2);
          removePointDefect(index1);
        }
        return;
      }
    }
  } else if (obj1->type() == Vacancy) {
    for (const auto& obj2 :
         _interstitialNeighborList->getNeighbors(obj1->position())) {
      double d = lattice()->calculateSquaredDistance(obj1->site().coord(),
                                                     obj2->site().coord());
      if (d < _squaredFrenkel) {
        size_t index1 = objectIndex(obj1);
        size_t index2 = objectIndex(obj2);
        if (index1 > index2) {
          removePointDefect(index1);
          removePointDefect(index2);
        } else {
          removePointDefect(index2);
          removePointDefect(index1);
        }
        return;
      }
    }
  }
}

/// Get status for commandline/thermo
std::string IrradiationModel::stepStatus() const {
  std::ostringstream buffer;
  if (algorithm()->step() == 0) {
    buffer << "   Step Time N(Vac) N(Int)" << std::endl;
  }

  buffer << std::setprecision(15) << algorithm()->step() << " " << std::setw(22)
         << algorithm()->time() << "  " << _vacancyCount << "  "
         << _interstitialCount << std::endl;
  return buffer.str();
}

/// Create a Vacancy on a site
void IrradiationModel::createVacancy(Site& site) {
  // make sure the new spot is empty
  if (site.object() != nullptr) {
    throw std::runtime_error("Tried to create a vacancy on an occupied site.");
  }

  // create vacancy
  auto vac = std::make_unique<VacancyIrradiation>(this, &site);

  // flag surrounding atoms for recalculation
  flagNeighbors(site);

  // Add to list
  _vacancyNeighborList->addDefect(vac.get());
  _eventObjects.push_back(std::move(vac));
  _vacancyCount++;
}

/// Create a Interstitial on a site
void IrradiationModel::createInterstitial(Site& site) {
  // make sure the new spot is empty
  if (site.object() != nullptr) {
    throw std::runtime_error(
        "Tried to create an interstitial on an occupied site.");
  }

  // create interstitial
  auto interstitial = std::make_unique<InterstitialObject>(this, &site);

  // flag surrounding atoms for recalculation
  flagNeighbors(site);

  // Add to list
  _interstitialNeighborList->addDefect(interstitial.get());
  _eventObjects.push_back(std::move(interstitial));
  _interstitialCount++;
}

/// Create a radiation source
void IrradiationModel::createRadiationSource(double dose,
                                             std::string radiationType) {
  if (dose <= 0) {
    std::cerr << "Error: Irradiation dose set to " << dose
              << " but needs to be > 0" << std::endl;
    exit(EXIT_FAILURE);
  }

  std::unique_ptr<EventObject> source;
  if (radiationType == "Irradiation") {
    source = std::make_unique<IrradiationObject<IrradiationModel>>(
        this, dose * _lattice->numSites());
  } else if (radiationType == "Vacancy") {
    source = std::make_unique<SpawnObject<IrradiationModel>>(
        this, dose * _lattice->numSites(), Vacancy);
  } else {
    source = std::make_unique<SpawnObject<IrradiationModel>>(
        this, dose * _lattice->numSites(), Interstitial);
  }

  _eventObjects.push_back(std::move(source));
}

/// Delete point defect
void IrradiationModel::removePointDefect(size_t index) {
  EventObject* object = _eventObjects[index].get();
  // Change defect counter
  if (object->type() == Vacancy) {
    _vacancyNeighborList->removeDefect(object);
    _vacancyCount--;
  } else if (object->type() == Interstitial) {
    _interstitialNeighborList->removeDefect(object);
    _interstitialCount--;
  } else {
    std::cerr << "Error: Object type " << object->type()
              << " passed to removePointDefect."
              << ". This should never happen!" << std::endl;
    exit(EXIT_FAILURE);
  }

  // flag surrounding point defects for recalculation
  flagNeighbors(object->site());

  // delete the defect from the list
  _eventObjects.erase(_eventObjects.begin() + index);
}

// flag surrounding atoms for recalculation
void flagNeighbors(const Site& site) {
  for (auto neighbor : site.neighbors()) {
    if (neighbor->type() == Vacancy || neighbor->type() == Interstitial) {
      EventObject* otherDefect = static_cast<EventObject*>(neighbor->object());
      otherDefect->setUpdateEvents(true);
    }
  }
}

bool typesMatchForRecombination(const EventObject& obj1,
                                const EventObject& obj2) {
  // Continue if it is not a vacancy and an interstitial
  if (!((obj1.type() == Vacancy && obj2.type() == Interstitial) ||
        (obj1.type() == Interstitial && obj2.type() == Vacancy))) {
    return false;
  }
  return true;
}
