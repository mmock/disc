///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "DiffusionModelYttrium.h"
#include "../objects/VacancyYttrium.h"

double DiffusionModelYttrium::jumpSwitchRate(const Site& vacancySite,
                                             const Site& targetSite) const {
  assert(_neighborShell == 1);
  int finalNeighborShell = neighborShell(vacancySite, targetSite);
  return _rates[1][finalNeighborShell];
}

void DiffusionModelYttrium::updateModel() {
  _neighborShell = neighborShell(_vacancy->site(), _solute->site());
  if (_neighborShell == 1) {
    _yttriumPosition = lattice()->getSaddle(solute()->site().coord(),
                                            _vacancy->site().coord());
  } else {
    _yttriumPosition = solute()->site().coord();
  }
}

void DiffusionModelYttrium::createVacancy(int siteIndex) {
  if (lattice()->sites()[siteIndex].type() == LatticeAtom) {
    _vacancy =
        std::make_unique<VacancyYttrium>(this, &lattice()->sites()[siteIndex]);
  } else {
    std::cerr << "Error: Vacancy site '" << siteIndex
              << "' is already occupied." << std::endl;
    exit(EXIT_FAILURE);
  }
}
