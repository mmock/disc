#include <time.h>
#include <unistd.h>
#include <libconfig.h++>
#include "../IO/Dump.h"
#include "../IO/MsgLogger.h"
#include "../IO/Thermo.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../analyzer/Analyzer.h"
#include "../models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = normal;

void printHelp() {
  std::cerr << std::endl;
  std::cerr << "KMC Code for diffusion problems" << std::endl;
  std::cerr << "Simulates diffusion in the presence of strain fields"
            << std::endl;
  std::cerr << "Author: Markus Mock <mock@mm.tu-darmstadt.de>" << std::endl;
  std::cerr << std::endl;
  std::cerr << "Usage: diffusion_kmc [options] inputfile" << std::endl;
  std::cerr << std::endl;
  std::cerr << "Options:" << std::endl;
  std::cerr << "-v: Verbose output (for debugging)" << std::endl;
  std::cerr << std::endl;
}

int main(int argc, char* argv[]) {
  // Save starting time
  clock_t tstart = clock();

  // Check command line flags.
  int c;
  while ((c = getopt(argc, argv, "vs")) != -1) switch (c) {
      case 'v':
        MsgLogger::setVerbosity(debug);
        break;
      case 's':
        MsgLogger::setVerbosity(none);
        break;
      case '?':
        printHelp();
        return 1;
      default:
        abort();
    }
  // Check if there is exactly one input file.
  if (argc - optind != 1) {
    printHelp();
    return 1;
  }

  char* configFilename = argv[optind];

  try {
    MsgLogger(normal) << "This is my KMC code " << std::endl;
    MsgLogger(normal) << "Reading config file " << configFilename << std::endl;
    // Initialize Model
    StrainModel model;
    model.parse(configFilename);
    // Run Simulation
    MsgLogger(normal) << separatorLine;
    MsgLogger(normal) << "Starting simulation: " << std::endl;
    // write initial configuration
    model.writeResults();
    while (model.algorithm()->step() < model.algorithm()->maximumSteps()) {
      model.takeStep();
    }
    // Write results
    for (auto& analyzer : model.analyzer()) {
      analyzer->writeResults();
    }
  } catch (std::string msg) {
    std::cerr << "ERROR: " << msg << std::endl;
    return 1;
  }

  // Done
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Calculation finished!" << std::endl;
  MsgLogger(normal) << "Runtime was "
                    << ((float)clock() - (float)tstart) / CLOCKS_PER_SEC << "s"
                    << std::endl;
  return 0;
}
