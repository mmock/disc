#include <time.h>
#include <unistd.h>
#include <libconfig.h++>
#include "../IO/Dump.h"
#include "../IO/MsgLogger.h"
#include "../IO/Thermo.h"
#include "../algorithms/KineticMonteCarlo.h"
#include "../analyzer/Analyzer.h"
#include "../events/JumpEvent.h"
#include "../models/StrainModel.h"

LoggerVerbosity MsgLogger::_globalLoggerVerbosity = normal;

void printHelp() {
  std::cerr << std::endl;
  std::cerr << "KMC Code for diffusion problems\n";
  std::cerr << "Calculates all jump rates in the system.\n";
  std::cerr << "Author: Markus Mock <mock@mm.tu-darmstadt.de>\n\n";
  std::cerr << "Usage: diffusion_kmc [options] inputfile\n\n";
  std::cerr << "Options:\n";
  std::cerr << "-v: Verbose output (for debugging)\n";
  std::cerr << std::endl;
}

int main(int argc, char* argv[]) {
  // Save starting time
  clock_t tstart = clock();

  // Check if there is exactly one input file.
  if (argc - optind != 1) {
    printHelp();
    return 1;
  }

  char* configFilename = argv[optind];

  try {
    MsgLogger(normal) << "This is my KMC code " << std::endl;
    MsgLogger(normal) << "Reading config file " << configFilename << std::endl;
    // Initialize Model
    StrainModel model;
    model.parse(configFilename);

    // Jump rates and energy changes of saddle points
    std::ofstream saddleFile;
    saddleFile.open("saddles.data");
    saddleFile << "initial final rateVac rateInt changeVac changeInt xx yy zz yz xz xy"
               << std::endl;

    // Energy changes of lattice points
    std::ofstream pointFile;
    pointFile.open("sites.data");
    pointFile << "index changeVac changeInt xx yy zz yz xz xy" << std::endl;

    Tensor startStrain, saddleStrain;
    for (auto site : model.lattice()->sites()) {
      // Calculate energy change at lattice position
      startStrain = model.strainField()->strain(site);
      double vacStartChange =
          model.energyChange(startStrain, model.vacancyDipoleTensorStart());
      double intStartChange =
          model.energyChange(startStrain, model.interstitialDipoleTensorStart());

      // Write results
      pointFile << site.atomIndex() << " " << vacStartChange << " "
                << intStartChange << " " << startStrain[0] << " "
                << startStrain[1] << " " << startStrain[2] << " "
                << startStrain[3] << " " << startStrain[4] << " "
                << startStrain[5] << std::endl;

      for (auto neighbor : site.neighbors()) {
        // Calculate energy change at saddle point
        saddleStrain = model.strainField()->saddleStrain(site, neighbor);
        double vacSaddleChange =
            model.energyChange(saddleStrain, model.vacancyDipoleTensorSaddle());
        double intSaddleChange =
            model.energyChange(saddleStrain, model.interstitialDipoleTensorSaddle());

        // Get rate change
        double rateVac = model.vacancyJumpRate(site, *neighbor);
        double rateInt = model.interstitialJumpRate(site, *neighbor);

        // Write results
        saddleFile << site.atomIndex() << " " << neighbor->atomIndex() << " "
                   << rateVac << " " << rateInt << " " << vacSaddleChange << " "
                   << intSaddleChange << " "  << saddleStrain[0] << " "  
                   << saddleStrain[1] << " "  << saddleStrain[2] << " "  
                   << saddleStrain[3] << " "  << saddleStrain[4] << " " 
                   << saddleStrain[5] << " "  << std::endl;
      }
    }
    if (model.dump()) {
      model.dump()->writeStep(*model.lattice(), model.algorithm()->step());
    }
  } catch (std::string msg) {
    std::cerr << "ERROR: " << msg << std::endl;
    return 1;
  }

  // Done
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Calculation finished!" << std::endl;
  MsgLogger(normal) << "Runtime was "
                    << ((float)clock() - (float)tstart) / CLOCKS_PER_SEC << "s"
                    << std::endl;
  return 0;
}
