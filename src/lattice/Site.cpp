///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Site.h"
#include "../objects/BasicObject.h"

const ObjectType Site::type() const {
  if (_object) {
    return _object->type();
  } else {
    return LatticeAtom;
  }
}

void Site::addNeighbor(Site* neigh, size_t shell) {
  assert(shell < _numNeighbors.size());

  size_t insertIndex = std::accumulate(
      _numNeighbors.begin(), _numNeighbors.begin() + shell, (size_t)0);
  _neighbors.insert(_neighbors.begin() + insertIndex, neigh);
  _numNeighbors[shell]++;
}

void Site::removeNeighbor(Site* neigh, size_t shell) {
  assert(shell < _numNeighbors.size());
  assert(std::find(_neighbors.begin(), _neighbors.end(), neigh) !=
         _neighbors.end());

  _neighbors.erase(std::find(_neighbors.begin(), _neighbors.end(), neigh));
  _numNeighbors[shell]--;
}

size_t Site::neighborIndex(const Site* neighbor) const {
  size_t pos = distance(_neighbors.begin(),
                        find(_neighbors.begin(), _neighbors.end(), neighbor));
  assert(pos < _neighbors.size());
  return pos;
}
