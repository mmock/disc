///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "../objects/ObjectType.h"

class BasicObject;

class Site {
 public:
  Site(const Vector3& coord, BasicObject* object = nullptr, int index = 0)
      : _coord(coord), _object(object), _atomIndex(index), _siteIndex(index) {}

  const Vector3& coord() const { return _coord; }
  const double x() const { return _coord[0]; }
  const double y() const { return _coord[1]; }
  const double z() const { return _coord[2]; }
  const ObjectType type() const;
  BasicObject* object() const { return _object; }
  const int atomIndex() const { return _atomIndex; }
  const int siteIndex() const { return _siteIndex; }
  const size_t shellCount() const { return _numNeighbors.size(); }
  const std::vector<Site*>& neighbors() const { return _neighbors; }
  const std::vector<size_t>& numNeighbors() const { return _numNeighbors; }
  const Site& neighbor(size_t neighborIndex) const {
    assert(neighborIndex < _neighbors.size());
    return *_neighbors[neighborIndex];
  }

  void setShellCount(size_t shells) { _numNeighbors.resize(shells, 0); }
  void setCoord(const Vector3& coord) { _coord = coord; }
  void setObject(BasicObject* object) { _object = object; }
  void setAtomIndex(int index) { _atomIndex = index; }

  void addNeighbor(Site* neigh, size_t shell);
  void removeNeighbor(Site* neigh, size_t shell);
  size_t neighborIndex(const Site* neighbor) const;

 protected:
  Vector3 _coord;
  BasicObject* _object;
  std::vector<Site*> _neighbors;
  std::vector<size_t> _numNeighbors;
  int _atomIndex;
  int _siteIndex;
};