///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "Site.h"

class Lattice {
 public:
  Lattice(const Vector3& cellSize)
      : _cellSize(cellSize), _halfCell(0.5 * cellSize) {}
  virtual ~Lattice() = default;

  /// get stuff
  std::vector<Site>& sites() { return _sites; }
  const std::vector<Site>& sites() const { return _sites; }
  size_t numSites() const { return _sites.size(); }
  const Vector3& cellSize() const { return _cellSize; }
  virtual size_t getIndex(const Vector3& position) = 0;

  // Calculate distance between two points with minimum image convention
  double calculateDistance(const Vector3& p1, const Vector3& p2) const {
    Vector3 delta = p1 - p2;
    mic(delta);
    return delta.norm();
  }

  // Calculate squared distance between two points with minimum image convention
  double calculateSquaredDistance(const Vector3& p1, const Vector3& p2) const {
    Vector3 delta = p1 - p2;
    mic(delta);
    return delta.squaredNorm();
  }

  // Get middle point between two coordinates
  Vector3 getSaddle(const Vector3& p1, const Vector3& p2) const {
    Vector3 delta = p2 - p1;
    mic(delta);
    delta = p1 + 0.5 * delta;
    wrapInCell(delta);
    return delta;
  }

  // Get the minimum length of vector vec in the cell
  void mic(Vector3& vec) const {
    // for (size_t i = 0; i < 3; ++i) {
    //   vec[i] -= std::round(vec[i] / _cellSize[i]) * _cellSize[i];
    // }
    vec = (vec.array() >= _halfCell.array()).select(vec - _cellSize, vec);
    vec = (vec.array() <= -_halfCell.array()).select(vec + _cellSize, vec);
  }

  // Wrap coordinates in cell
  void wrapInCell(Vector3& pos) const {
    pos = (pos.array() < 0).select(pos + _cellSize, pos);
    pos = (pos.array() >= _cellSize.array()).select(pos - _cellSize, pos);
  }

 private:
  // Setup Lattice
  virtual void buildNeighborList() = 0;
  virtual void setupLattice() = 0;

  /// List of lattice sites.
  std::vector<Site> _sites;
  /// Dimensions of the (periodic) simulation cell.
  Vector3 _cellSize;
  Vector3 _halfCell;
};
