///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "Lattice.h"

class BCCLattice : public Lattice {
 public:
  BCCLattice(const Vector3& cellSize) : Lattice(cellSize) {
    setupLattice();
    buildNeighborList();
  }

 private:
  void setupLattice() override {
    int atomIndex = 0;
    Vector3 pos, shift;
    shift << 0.5, 0.5, 0.5;
    sites().reserve(cellSize()[0] * cellSize()[1] * cellSize()[2]);
    for (int i = 0; i < cellSize()[0]; ++i) {
      for (int j = 0; j < cellSize()[1]; ++j) {
        for (int k = 0; k < cellSize()[2]; ++k) {
          pos = {(double)i, (double)j, (double)k};
          sites().push_back(Site(pos, nullptr, atomIndex++));
          sites().push_back(Site(pos + shift, nullptr, atomIndex++));
        }
      }
    }
  }

  void buildNeighborList() override {
    for (Site& site : sites()) {
      double x, y, z;
      x = site.x();
      y = site.y();
      z = site.z();
      site.setShellCount(1);
      site.addNeighbor(&sites()[getIndex({x + 0.5, y + 0.5, z + 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x + 0.5, y + 0.5, z - 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x + 0.5, y - 0.5, z + 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x + 0.5, y - 0.5, z - 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x - 0.5, y + 0.5, z + 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x - 0.5, y + 0.5, z - 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x - 0.5, y - 0.5, z + 0.5})], 0);
      site.addNeighbor(&sites()[getIndex({x - 0.5, y - 0.5, z - 0.5})], 0);
    }
  }

  size_t getIndex(const Vector3& pos) override {
    // Make sure the position is in the box
    Vector3 position = Vector3(pos);
    wrapInCell(position);
    // Determine unit cell number
    size_t unitCellNumber, atomNumber;
    double xi, yi, zi;

    unitCellNumber = std::floor(position[0]) * cellSize()[1] * cellSize()[2] +
                     std::floor(position[1]) * cellSize()[2] +
                     std::floor(position[2]);

    // Determine fractional coordinates in unit cell
    xi = position[0] - std::floor(position[0]);
    yi = position[1] - std::floor(position[1]);
    zi = position[2] - std::floor(position[2]);

    if (xi == 0 && yi == 0 && zi == 0) {
      atomNumber = 0;
    } else if (xi == 0.5 && yi == 0.5 && zi == 0.5) {
      atomNumber = 1;
    } else {
      std::cerr << "ERROR: The Point (" << xi << " " << yi << " " << zi
                << ") is no viable internal coordinate for a bcc unit cell."
                << std::endl;
      exit(EXIT_FAILURE);
    }

    return 2 * unitCellNumber + atomNumber;
  }
};
