///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "../objects/EventObject.h"

class NeighborList {
  typedef std::array<size_t, 3> Indices;

 public:
  NeighborList(const Vector3& boxSize, double radius,
               const std::vector<std::unique_ptr<EventObject>>& objects,
               ObjectType type)
      : _type(type) {
    createCells(boxSize, radius);

    // Sort defects into cells
    for (auto& object : objects) {
      if (object->type() != _type) continue;
      addDefect(object.get());
    }
  }

  std::vector<EventObject*> getNeighbors(const Vector3& position) const {
    std::vector<EventObject*> neighbors;
    Indices indices = cellIndices(position);

    // Collect all defects in all 27 neighbor cells while remembering PBC.
    for (int i = -1; i < 2; ++i) {
      int tmp = indices[0] + i;
      size_t x = wrap(tmp, _cellNumber[0]);
      for (int j = -1; j < 2; ++j) {
        size_t y = wrap(indices[1] + j, _cellNumber[1]);
        for (int k = -1; k < 2; ++k) {
          size_t z = wrap(indices[2] + k, _cellNumber[2]);
          neighbors.insert(neighbors.end(),
                           _cells[indicesToIndex(x, y, z)].begin(),
                           _cells[indicesToIndex(x, y, z)].end());
        }
      }
    }

    return neighbors;
  }

  void updateCell(EventObject* object, const Vector3& oldPosition) {
    size_t oldIndex = calculateIndex(oldPosition);
    size_t newIndex = calculateIndex(object->position());
    if (oldIndex == newIndex) return;

    _cells[oldIndex].erase(
        std::remove(_cells[oldIndex].begin(), _cells[oldIndex].end(), object),
        _cells[oldIndex].end());
    _cells[newIndex].push_back(object);
  }

  void addDefect(EventObject* defect) {
    assert(defect->type() == _type);
    _cells[calculateIndex(defect->position())].push_back(defect);
  }

  void removeDefect(EventObject* defect) {
    size_t index = calculateIndex(defect->position());
    _cells[index].erase(
        std::remove(_cells[index].begin(), _cells[index].end(), defect),
        _cells[index].end());
  }

 private:
  void createCells(const Vector3& boxSize, double radius) {
    // Determine number of cells
    for (size_t i = 0; i < 3; ++i) {
      double n = std::floor(boxSize[i] / radius);
      _cellNumber[i] = n;
      _cellLength[i] = boxSize[i] / n;
    }
    // Create cells
    size_t ntotal = _cellNumber[0] * _cellNumber[1] * _cellNumber[2];
    _cells.resize(ntotal, std::vector<EventObject*>());
  }

  size_t calculateIndex(const Vector3& position) const {
    Indices indices = cellIndices(position);
    return indicesToIndex(indices[0], indices[1], indices[2]);
  }

  size_t indicesToIndex(size_t i, size_t j, size_t k) const {
    size_t index = i * _cellNumber[0] * _cellNumber[0] + j * _cellNumber[1] + k;
    assert(index < _cells.size());
    return index;
  }

  Indices cellIndices(const Vector3& position) const {
    Indices indices;
    for (size_t i = 0; i < 3; ++i) {
      indices[i] = std::floor(position[i] / (_cellNumber[i] * _cellLength[i]));
    }
    return indices;
  }

  size_t wrap(int index, int limit) const {
    assert(limit > index);
    if (index < 0) {
      return limit - 1;
    } else if (index == limit) {
      return 0;
    } else {
      return index;
    }
  }

  std::vector<std::vector<EventObject*>> _cells;
  Vector3 _cellNumber;
  Vector3 _cellLength;
  ObjectType _type;
};
