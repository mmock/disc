///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

/******************************************************************************
 * STL Library
 ******************************************************************************/
#include <math.h>
#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <memory>
#include <random>
#include <sstream>
#include <string>
#include <vector>

/******************************************************************************
 * libconfig library
 ******************************************************************************/
#include <libconfig.h++>

/******************************************************************************
 * Eigen3 library
 ******************************************************************************/
#include <Eigen/Dense>
typedef Eigen::Vector3d Vector3;
typedef Eigen::Matrix<double, 6, 1> Tensor;
typedef Eigen::Matrix<double, 3, 3> TensorFull;

/******************************************************************************
 * Standard set of our own headers
 ******************************************************************************/
#include "IO/MsgLogger.h"
#include "IO/config.h"