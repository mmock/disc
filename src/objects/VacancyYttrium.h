///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/JumpEvent.h"
#include "../events/JumpSwitchEvent.h"
#include "../lattice/Site.h"
#include "../models/DiffusionModelYttrium.h"
#include "EventObject.h"

class VacancyYttrium : public EventObject {
 public:
  /// Constructor
  VacancyYttrium(DiffusionModelYttrium* model, Site* site)
      : EventObject(site, Vacancy), _model(model) {}

  // Create the list of all jump events
  void createEventList() override {
    events().clear();
    for (auto neighbor : site().neighbors()) {
      if (neighbor->type() == Solute) {
        for (auto yttriumNeighbor : neighbor->neighbors()) {
          if (yttriumNeighbor->type() == Vacancy) {
            continue;
          }
          double rate = _model->jumpSwitchRate(*_site, *yttriumNeighbor);
          auto event = std::make_unique<JumpSwitchEvent>(
              _site, neighbor->object(), yttriumNeighbor, rate);
          events().push_back(std::move(event));
        }
      } else {
        double rate = _model->vacancyJumpRate(_site, neighbor);
        auto event = std::make_unique<JumpEvent>(_site, neighbor, rate);
        events().push_back(std::move(event));
      }
    }
    setUpdateEvents(false);
  }

 private:
  DiffusionModelYttrium* _model;
};
