///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/SpawnEvent.h"
#include "EventObject.h"

template <class ModelType>
class SpawnObject : public EventObject {
 public:
  /// Constructor
  SpawnObject(ModelType* model, double dose, ObjectType type)
      : EventObject(nullptr, IrradiationSource),
        _model(model),
        _dose(dose),
        _type(type) {}

  // Create the list of all jump events
  void createEventList() override {
    events().clear();
    events().push_back(std::make_unique<SpawnEvent>(_model, _dose, _type));
    assert(events().size() == 1);
    setUpdateEvents(false);
  }

  /// Returns the Radiation dose
  double dose() { return _dose; }

 private:
  ModelType* _model;
  /// Radiation dose * numSites
  double _dose;
  ObjectType _type;
};
