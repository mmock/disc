///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/DestructionEvent.h"
#include "../events/JumpEventIrradiation.h"
#include "../lattice/Site.h"
#include "../models/IrradiationModel.h"
#include "EventObject.h"

class VacancyIrradiation : public EventObject {
 public:
  /// Constructor
  VacancyIrradiation(IrradiationModel* model, Site* site)
      : EventObject(site, Vacancy), _model(model) {}

  // Create the list of all jump events
  void createEventList() override {
    events().clear();
    for (auto neighbor : site().neighbors()) {
      if (neighbor->type() == _type) {
        // Exchanging two identical objects makes no sense.
        continue;
      }

      double rate = _model->vacancyJumpRate(*_site, *neighbor);
      if (neighbor->type() == Precipitate) {
        auto event = std::make_unique<DestructionEvent>(_model, this, rate);
        events().push_back(std::move(event));
      } else {
        auto event = std::make_unique<JumpEventIrradiation>(_model, this, _site,
                                                            neighbor, rate);
        events().push_back(std::move(event));
      }
    }
    setUpdateEvents(false);
  }

 private:
  IrradiationModel* _model;
};
