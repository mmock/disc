///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../events/Event.h"
#include "BasicObject.h"

class Site;

class EventObject : public BasicObject {
 public:
  EventObject(Site* site, ObjectType type)
      : BasicObject(site, type), _updateFlag(true){};

  std::vector<std::unique_ptr<Event>>& events() { return _events; }
  bool shouldUpdateEvents() { return _updateFlag; }

  void setUpdateEvents(bool updateFlag) { _updateFlag = updateFlag; }

  virtual void createEventList() = 0;

  void move(Site* target) override {
    BasicObject::move(target);
    setUpdateEvents(true);
  }

 private:
  std::vector<std::unique_ptr<Event>> _events;
  bool _updateFlag;
};
