///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "../lattice/Site.h"
#include "ObjectType.h"

class BasicObject {
 public:
  /// Constructor
  BasicObject(Site* site, ObjectType type) : _site(site), _type(type) {
    if (_site) {
      _site->setObject(this);
    }
  }

  // copy constructor
  BasicObject(const BasicObject& other) = delete;
  // copy assignment operator
  BasicObject& operator=(const BasicObject& other) = delete;

  /// Destructor
  ~BasicObject() {
    if (_site) {
      _site->setObject(nullptr);
    }
  }

  Site* siteptr() { return _site; }
  const Site& site() { return *_site; }
  const Site& site() const { return *_site; }
  const Vector3& position() const { return _site->coord(); }
  const ObjectType type() const { return _type; }

  void setSite(Site* site) { _site = site; }

  virtual void move(Site* target) {
    BasicObject* otherObject = target->object();
    // update the atom indices
    int index = target->atomIndex();
    target->setAtomIndex(_site->atomIndex());
    _site->setAtomIndex(index);
    // update the object positions
    target->setObject(this);
    if (otherObject) {
      // Move the object on the initial site
      otherObject->setSite(_site);
    }
    // Update the old site (if it was empty otherObject == nullptr)
    _site->setObject(otherObject);
    // update the site to the target site
    _site = target;
  }

 protected:
  ObjectType _type;
  Site* _site;
};
