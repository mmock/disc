///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "../lattice/Lattice.h"
#include "../lattice/Site.h"
#include "OctreeNode.h"
#include "StrainField.h"

class SystemStrain;
class Lattice;

class FEMStrainField : public StrainField {
 public:
  /// Default constructor
  FEMStrainField() : StrainField(){};

  /// Constructor
  FEMStrainField(const std::unique_ptr<Lattice>& lattice,
                 const libconfig::Setting& strain);

  const std::vector<Vector3>& nodes() const { return _nodes; }

  void parseOptions(const libconfig::Setting& strain);
  void parseFEM(std::string& fileName);
  void parseElement(const std::string& line);

  Tensor calculateStrain(const Vector3& originalPoint) const override;
  Vector3 coordTransform(const Vector3& originalPoint) const;

 private:
  double _strainFactor;
  std::string _fileName;

  /// Necessary for transformation between simulation and strain field
  /// coordinates
  Vector3 _cell;

  /// The edges of the box that contain the FEM mesh.
  // The mesh will often be centered on (0,0,0) and go from (-0.5,-0.5,-0.5) to
  // (0.5,0.5,0.5)
  Vector3 _edgeA;
  Vector3 _edgeB;

  /// The knots of the mesh
  std::vector<Vector3> _nodes;
  std::unique_ptr<Node> _octree;

  /// Strain tensor at the Nodes from FEM
  /// Voigt notation: [eps_xx, eps_yy, eps_zz, 2*eps_yz, 2*eps_xz, 2*eps_xy]
  std::vector<Tensor> _strainNodes;

  // Determines if the file only contains a octant of the strain field.
  bool _octant;

  // Hydrostatic strain due to finite box size in FEM calculations
  double _hydrostatic;
};
