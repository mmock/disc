///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "Element.h"

class StrainField;

class Tetrahedron10 : public Element {
 public:
  /// Constructor
  Tetrahedron10() : Element(){};
  Tetrahedron10(const std::vector<size_t>& nodeIndices,
                const std::vector<Vector3>& nodes)
      : Element(nodeIndices, nodes) {}

  std::vector<double> formFunction(const Vector3& point) const override {
    std::vector<double> formFunction(10);

    double r = point[0];
    double s = point[1];
    double t = point[2];
    double u = 1 - r - s - t;

    formFunction[0] = u * (2 * u - 1);
    formFunction[1] = r * (2 * r - 1);
    formFunction[2] = s * (2 * s - 1);
    formFunction[3] = t * (2 * t - 1);
    formFunction[4] = 4 * u * r;
    formFunction[5] = 4 * r * s;
    formFunction[6] = 4 * s * u;
    formFunction[7] = 4 * t * u;
    formFunction[8] = 4 * r * t;
    formFunction[9] = 4 * s * t;

    return formFunction;
  }
};
