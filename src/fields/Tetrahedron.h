///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "Element.h"

class StrainField;

class Tetrahedron : public Element {
 public:
  /// Constructor
  Tetrahedron() : Element(){};
  Tetrahedron(const std::vector<size_t>& nodeIndices,
              const std::vector<Vector3>& nodes)
      : Element(nodeIndices, nodes) {}

  std::vector<double> formFunction(const Vector3& point) const override {
    std::vector<double> formFunction(4);
    formFunction[0] = 1 - point[0] - point[1] - point[2];
    formFunction[1] = point[0];
    formFunction[2] = point[1];
    formFunction[3] = point[2];
    return formFunction;
  }
};
