///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "FEMStrainField.h"
#include "Tetrahedron.h"
#include "Tetrahedron10.h"

/// Constructor
FEMStrainField::FEMStrainField(const std::unique_ptr<Lattice>& lattice,
                               const libconfig::Setting& strain)
    : StrainField(lattice) {
  _cell = lattice->cellSize();
  parseOptions(strain);
  _octree = std::make_unique<Node>(nullptr, _edgeA, _edgeB);
  parseFEM(_fileName);
}

void FEMStrainField::parseOptions(const libconfig::Setting& strain) {
  _strainFactor = parseOptionalDouble(strain, "StrainFactor", 1.0);
  MsgLogger(normal) << "StrainFactor: " << _strainFactor << std::endl;

  _fileName = parseString(strain, "FileName");
  _edgeA = parseVector(strain, "GridEdgeA");
  _edgeB = parseVector(strain, "GridEdgeB");
  _octant = parseOptionalBool(strain, "Octant", false);
  _hydrostatic = parseOptionalDouble(strain, "HydrostaticStrain", 0.0);
  MsgLogger(normal) << "Octant: " << _octant << std::endl;
  MsgLogger(normal) << "Hydrostatic strain correction: " << _hydrostatic
                    << std::endl;
}

/// Parser
void FEMStrainField::parseFEM(std::string& fileName) {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing strain field:" << std::endl;

  std::ifstream f(fileName);
  if (!f.good()) {
    throw std::string("Input file not found.");
  }

  Vector3 point;
  Tensor tensor;
  enum Status { skip, nodes, elements, strain };
  Status status = skip;
  double n;
  while (!f.eof()) {
    std::string buffer;
    getline(f, buffer);

    // determine which part of the file we are reading
    if (buffer.empty()) {
      status = skip;
      continue;
    } else if (buffer.find("POINTS") != std::string::npos) {
      MsgLogger(normal) << "Parsing Knots" << std::endl;
      status = nodes;
      continue;
    } else if (buffer.find("CELLS") != std::string::npos) {
      MsgLogger(normal) << "Parsing Cells" << std::endl;
      status = elements;
      continue;
    } else if (buffer.find("CartesianStrain") != std::string::npos ||
               buffer.find("epsx") != std::string::npos) {
      MsgLogger(normal) << "Parsing Strain Tensor" << std::endl;
      status = strain;
      continue;
    }

    if (status == skip) {
      continue;
    } else if (status == nodes) {
      // Parse Nodes of the mesh
      std::stringstream stream(buffer);
      int i = 0;
      while (stream >> n) {
        point[i] = n;
        i++;
      }
      _nodes.push_back(point);
    } else if (status == elements) {
      // Parse which nodes form an element
      parseElement(buffer);
    } else if (status == strain) {
      // Parse Strain Tensor
      std::stringstream stream(buffer);
      int i = 0;
      while (i < 6 && stream >> n) {
        tensor[i] = n;
        i++;
      }
      // Reorder strain tensor from
      // [eps_xx, eps_yy, eps_zz, 2*eps_xy, 2*eps_yz, 2*eps_xz]
      // to the Voigt notation
      // [eps_xx, eps_yy, eps_zz, 2*eps_yz, 2*eps_xz, 2*eps_xy]
      double xy = tensor[3];
      tensor[3] = tensor[4];
      tensor[4] = tensor[5];
      tensor[5] = xy;
      _strainNodes.push_back(tensor);
    }
  }
}

void FEMStrainField::parseElement(const std::string& line) {
  std::stringstream stream(line);
  std::vector<size_t> indices;
  int numNodes = -1;
  double n;

  // Get number of nodes and collect indices
  while (stream >> n) {
    if (numNodes == -1) {
      numNodes = n;
      continue;
    }
    indices.push_back(n);
  }

  // Collect coordinates
  std::vector<Vector3> nodeCoordinates;
  for (size_t i = 0; i < numNodes; ++i) {
    nodeCoordinates.push_back(_nodes[indices[i]]);
  }

  // Create and store element
  if (numNodes == 4) {
    auto element = std::make_shared<Tetrahedron>(indices, nodeCoordinates);
    _octree->add(element);
  } else if (numNodes == 10) {
    auto element = std::make_shared<Tetrahedron10>(indices, nodeCoordinates);
    _octree->add(element);
  } else {
    throw std::string("Invalid number of nodes.");
  }
}

/// Transform simulation coordinates to strain coordinates
Vector3 FEMStrainField::coordTransform(const Vector3& originalPoint) const {
  Vector3 point;
  if (_octant) {
    point = ((originalPoint.array() / _cell.array()) - 0.5).abs() * 2 *
                (_edgeB.array() - _edgeA.array()) +
            _edgeA.array();
  } else {
    point = originalPoint.array() / _cell.array() *
                (_edgeB.array() - _edgeA.array()) +
            _edgeA.array();
  }
  return point;
}

/// Calculate strain at one point in simulation coordinates
Tensor FEMStrainField::calculateStrain(const Vector3& originalPoint) const {
  // get point in strain field coordinates.
  Vector3 point = coordTransform(originalPoint);

  // get index and internal coord of tetrahedron that contains the point
  Element* element = _octree->findElement(point);
  std::vector<double> formFunction = element->getFormFunction(point);
  std::vector<size_t> nodeIndices = element->nodeIndices();

  Tensor strain;
  for (size_t i = 0; i < 6; ++i) {
    strain[i] = 0;
    for (size_t n = 0; n < formFunction.size(); ++n) {
      strain[i] +=
          formFunction[n] * _strainNodes[nodeIndices[n]][i] * _strainFactor;
    }
  }

  // Correct for hydrostatic residue
  for (size_t i = 0; i < 3; ++i) {
    strain[i] += _hydrostatic * _strainFactor / 3.0;
  }

  return strain;
}
