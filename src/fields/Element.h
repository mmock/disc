///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"

class StrainField;

class Element {
 public:
  /// Constructor
  Element(){};
  Element(const std::vector<size_t>& nodeIndices,
          const std::vector<Vector3>& nodes)
      : _nodeIndices(nodeIndices), _nodes(nodes) {
    // Calculate bounding box of tetrahedron
    for (size_t i = 0; i < 3; ++i) {
      _min[i] = nodes[0][i];
      _max[i] = nodes[0][i];
      for (size_t j = 1; j < nodes.size(); ++j) {
        _min[i] = std::min(_min[i], nodes[j][i]);
        _max[i] = std::max(_max[i], nodes[j][i]);
      }
    }

    // Calculate inverse of node matrix for internal coordinates
    Eigen::Matrix3d m;
    m << nodes[1][0] - nodes[0][0], nodes[2][0] - nodes[0][0],
        nodes[3][0] - nodes[0][0], nodes[1][1] - nodes[0][1],
        nodes[2][1] - nodes[0][1], nodes[3][1] - nodes[0][1],
        nodes[1][2] - nodes[0][2], nodes[2][2] - nodes[0][2],
        nodes[3][2] - nodes[0][2];
    _inv = m.inverse();

    // Calculate normal of every side
    _normals.push_back((_nodes[1] - _nodes[0]).cross(_nodes[2] - _nodes[0]));
    _normals.push_back((_nodes[2] - _nodes[1]).cross(_nodes[3] - _nodes[1]));
    _normals.push_back((_nodes[3] - _nodes[2]).cross(_nodes[0] - _nodes[2]));
    _normals.push_back((_nodes[0] - _nodes[3]).cross(_nodes[1] - _nodes[3]));

    _dotV4.push_back(_normals[0].dot(_nodes[3] - _nodes[0]));
    _dotV4.push_back(_normals[1].dot(_nodes[0] - _nodes[1]));
    _dotV4.push_back(_normals[2].dot(_nodes[1] - _nodes[2]));
    _dotV4.push_back(_normals[3].dot(_nodes[2] - _nodes[3]));
  }

  virtual ~Element() = default;

  const std::vector<size_t>& nodeIndices() const { return _nodeIndices; }
  const Vector3& min() const { return _min; }
  const Vector3& max() const { return _max; }

  virtual std::vector<double> formFunction(const Vector3& point) const = 0;

  // Calculate internal coordinates and the form function
  std::vector<double> getFormFunction(const Vector3& point) {
    Vector3 internal = internalCoordinates(point);
    // Small epsilon as points directly on the edge may have internal
    // coordinates on the order of -1e-17.
    assert((internal.array() >= 0.0 - 1e-15).all() &&
           (internal.array() <= 1.0 + 1e-15).all());
    return formFunction(internal);
  }

  bool inTetrahedron(const Vector3& point) const {
    // https://stackoverflow.com/questions/25179693/how-to-check-whether-the-point-is-in-the-tetrahedron-or-not/25180294
    return inBoundingBox(point) && sameSide(point, 0) && sameSide(point, 1) &&
           sameSide(point, 2) && sameSide(point, 3);
  }

  bool inBoundingBox(const Vector3& point) const {
    for (size_t i = 0; i < 3; ++i) {
      if (point[i] < _min[i] || point[i] > _max[i]) {
        return false;
      }
    }
    return true;
  }

  bool sameSide(const Vector3& point, size_t side) const {
    // Checks if the point is on the same side or inside of the plane of the
    // tetrahedron as the fourth corner
    double a = _normals[side].dot(point - _nodes[side]);
    return !a || ((a < 0) == (_dotV4[side] < 0));
  }

  Vector3 internalCoordinates(const Vector3& point) const {
    Vector3 r;
    r = _inv * (point - _nodes[0]);
    return r;
  }

 protected:
  std::vector<size_t> _nodeIndices;
  std::vector<Vector3> _nodes;

  std::vector<Vector3> _normals;
  std::vector<double> _dotV4;

  Vector3 _min;
  Vector3 _max;

  Eigen::Matrix3d _inv;
};
