///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "../lattice/Lattice.h"
#include "../lattice/Site.h"

class SystemStrain;
class Lattice;

class StrainField {
 public:
  /// Default constructor and destructor
  StrainField(){};
  StrainField(const std::unique_ptr<Lattice>& lattice) {
    _lattice = lattice.get();
  };
  virtual ~StrainField() = default;

  const Tensor& strain(const Site& site) const {
    return _strainPoints[site.siteIndex()];
  }
  const Tensor& saddleStrain(const Site& site, Site* end) const {
    return _strainSaddles[site.siteIndex()][site.neighborIndex(end)];
  }

  virtual Tensor calculateStrain(const Vector3& point) const = 0;

  /// Calculate strain at all lattice and saddle points
  void calculateStrainField() {
    const auto& sites = _lattice->sites();
    _strainPoints.reserve(sites.size());
    _strainSaddles.resize(sites.size(),
                          std::vector<Tensor>(sites[0].numNeighbors()[0]));
    for (const auto& site : sites) {
      _strainPoints.push_back(calculateStrain(site.coord()));
      for (const auto& neighbor : site.neighbors()) {
        if (site.siteIndex() > neighbor->siteIndex()) continue;
        Vector3 saddle = _lattice->getSaddle(site.coord(), neighbor->coord());
        Tensor strain = calculateStrain(saddle);
        _strainSaddles[site.siteIndex()][site.neighborIndex(neighbor)] = strain;
        _strainSaddles[neighbor->siteIndex()][neighbor->neighborIndex(&site)] =
            strain;
      }
    }
  }

 protected:
  Lattice* _lattice;

  /// Strain tensor at lattice and saddle points
  std::vector<std::vector<Tensor>> _strainSaddles;
  std::vector<Tensor> _strainPoints;
};
