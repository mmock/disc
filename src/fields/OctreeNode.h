///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "Element.h"

class Node {
 public:
  Node(Node* parent, Vector3 min, Vector3 max)
      : _parent(parent), _min(min), _max(max) {}

  void add(const std::shared_ptr<Element>& element) {
    if (_children.empty()) {
      _elements.push_back(element);
      if (_elements.size() >= _size) {
        spawnChildren();
        distributeObjects();
      }
    } else {
      for (auto& child : _children) {
        if (child->contains(element)) {
          child->add(element);
        }
      }
    }
  }

  void spawnChildren() {
    Vector3 mid = (_min.array() + _max.array()) / 2;
    _children.push_back(std::make_unique<Node>(this, _min, mid));
    Vector3 min = {mid[0], _min[1], _min[2]};
    Vector3 max = {_max[0], mid[1], mid[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    min = {_min[0], mid[1], _min[2]};
    max = {mid[0], _max[1], mid[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    min = {mid[0], mid[1], _min[2]};
    max = {_max[0], _max[1], mid[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    min = {_min[0], _min[1], mid[2]};
    max = {mid[0], mid[1], _max[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    min = {mid[0], _min[1], mid[2]};
    max = {_max[0], mid[1], _max[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    min = {_min[0], mid[1], mid[2]};
    max = {mid[0], _max[1], _max[2]};
    _children.push_back(std::make_unique<Node>(this, min, max));
    _children.push_back(std::make_unique<Node>(this, mid, _max));
  }

  void distributeObjects() {
    for (auto& element : _elements) {
      for (auto& child : _children) {
        if (child->contains(element)) {
          child->add(element);
        }
      }
    }
    _elements.clear();
    _elements.shrink_to_fit();
  }

  template <class T>
  Node* determineChild(const T& obj) const {
    for (auto& child : _children) {
      if (child->contains(obj)) {
        return child.get();
      }
    }
    assert(false);
  }

  bool contains(const std::shared_ptr<Element>& element) const {
    for (size_t i = 0; i < 3; ++i) {
      if (element->max()[i] < _min[i] || element->min()[i] > _max[i]) {
        return false;
      }
    }
    return true;
  }

  bool contains(const Vector3& point) const {
    for (size_t i = 0; i < 3; ++i) {
      if (point[i] < _min[i] || point[i] > _max[i]) {
        return false;
      }
    }
    return true;
  }

  Element* findElement(const Vector3& point) const {
    if (_children.empty()) {
      for (auto const& element : _elements) {
        if (element->inTetrahedron(point)) {
          return element.get();
        }
      }
      assert(false);
    } else {
      return determineChild(point)->findElement(point);
    }
  }

 private:
  Node* _parent;
  std::vector<std::unique_ptr<Node>> _children;
  std::vector<std::shared_ptr<Element>> _elements;

  // Maximum number of elements before children are spawned
  // An extremly distorted mesh could in theory have more than
  // 500 elements sharing one corner. This would lead to an
  // infinite splitting and use up all memory.
  size_t _size = 500;

  Vector3 _min;
  Vector3 _max;
};
