///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"
#include "StrainField.h"

class SystemStrain;
class Lattice;

class AnalyticalStrainField : public StrainField {
 public:
  /// Default constructor
  AnalyticalStrainField() : StrainField(){};

  /// Constructor
  AnalyticalStrainField(const std::unique_ptr<Lattice> &lattice,
                        const libconfig::Setting &strain)
      : StrainField() {
    _lattice = lattice.get();
    parseOptions(strain);
  };

  void parseOptions(const libconfig::Setting &strain) {
    MsgLogger(normal) << separatorLine;
    MsgLogger(normal) << "Parsing Analytical strain field:" << std::endl;
    _center = parseVector(strain, "PrecipitateCenter");

    double b = parseDouble(strain, "PrecipitateBulkModulus");
    double g = parseDouble(strain, "MatrixShearModulus");
    double missfit = parseDouble(strain, "MisfitStrain");
    double r = parseDouble(strain, "PrecipitateRadius");

    double v0 = 4.0 / 3.0 * M_PI * std::pow(r, 3);
    double dV = v0 * missfit;
    // Formula 4.68 Wei Cai
    _dVa = dV / (1 + ((4.0 * g) / (3.0 * b)));
    MsgLogger(normal) << "dVa: " << _dVa << std::endl;
  }

  /// Calculate strain at one point in simulation coordinates
  Tensor calculateStrain(const Vector3 &point) const override {
    Tensor strainVoigt;
    if (((point - _center).array() == 0.0).all()) {
      // set strain at the center to 0
      strainVoigt << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
      return strainVoigt;
    }
    Vector3 sphere = sphericalCoordinates(point);

    // Formula 4.42 Wei Cai
    double err = -_dVa / (2.0 * M_PI * std::pow(sphere[0], 3));
    double epp = _dVa / (4.0 * M_PI * std::pow(sphere[0], 3));
    TensorFull strainSpherical;
    strainSpherical << err, 0.0, 0.0, 0.0, epp, 0.0, 0.0, 0.0, epp;
    TensorFull strainCart = cartesianTensor(sphere, strainSpherical);

    // [eps_xx, eps_yy, eps_zz, 2*eps_yz, 2*eps_xz, 2*eps_xy]
    strainVoigt << strainCart(0, 0), strainCart(1, 1), strainCart(2, 2),
        2 * strainCart(1, 2), 2 * strainCart(0, 2), 2 * strainCart(0, 1);

    assert(isfinite(strainVoigt.array()).all());

    return strainVoigt;
  }

  /// Coordinate transformation from cartesian to spherical coordinates
  Vector3 sphericalCoordinates(const Vector3 &point) const {
    // Coordinates with respect to the center of the precipitate
    Vector3 p = point - _center;

    Vector3 sphere;
    double r = p.norm();
    sphere[0] = r;
    sphere[1] = std::atan2(p[1], p[0]);
    sphere[2] = std::acos(p[2] / r);

    assert(isfinite(sphere.array()).all());
    return sphere;
  }

  /// Coordinate transformation of strain tensor from spherical to cartesian
  /// coordinates
  TensorFull cartesianTensor(const Vector3 &sphere,
                             const TensorFull &sphericalStrain) const {
    TensorFull transformation;
    double phi = sphere[1];
    double tta = sphere[2];

    // http://solidmechanics.org/text/AppendixD/AppendixD.htm
    // D.1.7 Converting tensors between Cartesian and Spherical-Polar bases
    transformation << std::sin(tta) * std::cos(phi),
        std::cos(tta) * std::cos(phi), -std::sin(phi),
        std::sin(tta) * std::sin(phi), std::cos(tta) * std::sin(phi),
        std::cos(phi), std::cos(tta), -std::sin(tta), 0.0;

    return transformation * sphericalStrain * transformation.transpose();
  }

 private:
  // Volume expansion of hole after insertion of precipitate
  double _dVa;
  // Center of the precipitate
  Vector3 _center;
};