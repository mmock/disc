///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "Dump.h"

Dump::Dump(const libconfig::Setting& setting) { parse(setting); }

Dump::~Dump() {}

void Dump::parse(const libconfig::Setting& setting) {
  MsgLogger(debug) << separatorLine;
  MsgLogger(debug) << "Parsing Dump:" << std::endl;

  // parse setting
  _dumpFrequency = parseInt(setting, "Frequency");
  MsgLogger(debug) << "Frequency: " << _dumpFrequency << std::endl;
  _fileBase = parseString(setting, "FileName");
  MsgLogger(debug) << "FileName: " << _fileBase << ".StepNumber" << std::endl;
}

void Dump::writeStep(const Lattice& lattice, int step) {
  if (step % _dumpFrequency != 0) {
    return;
  }
  _path = _fileBase + "." + std::to_string(step);

  _dump.open(_path);
  if (!_dump.good()) {
    std::cerr << "Error: Unable to create dump file '" << _path << "'"
              << std::endl;
    exit(EXIT_FAILURE);
  }

  _dump << "ITEM: TIMESTEP" << std::endl;
  _dump << step << std::endl;
  _dump << "ITEM: NUMBER OF ATOMS" << std::endl;
  _dump << lattice.numSites() << std::endl;
  _dump << "ITEM: BOX BOUNDS pp pp pp" << std::endl;
  _dump << "0 " << lattice.cellSize()[0] << std::endl;
  _dump << "0 " << lattice.cellSize()[1] << std::endl;
  _dump << "0 " << lattice.cellSize()[2] << std::endl;
  _dump << "ITEM: ATOMS id type x y z" << std::endl;
  for (auto site : lattice.sites()) {
    _dump << site.atomIndex() + 1 << " " << site.type() << " " << site.x()
          << " " << site.y() << " " << site.z() << std::endl;
  }
  _dump.close();
}
