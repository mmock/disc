///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"

/// Parse required setting from config
libconfig::Setting& parseSetting(const libconfig::Setting& cfg,
                                 std::string settingName);

/// Parse optional boolean from config
bool parseOptionalBool(const libconfig::Setting& cfg, std::string settingName,
                       bool defaultValue);

/// Parse required double from config
double parseDouble(const libconfig::Setting& cfg, std::string settingName);

/// Parse optional double from config
double parseOptionalDouble(const libconfig::Setting& cfg,
                           std::string settingName, double defaultValue);

/// Parse required integer from config
int64_t parseInt(const libconfig::Setting& cfg, std::string settingName);

/// Parse required string from config
std::string parseString(const libconfig::Setting& cfg, std::string settingName);

/// Parse optional string from config
std::string parseOptionalString(const libconfig::Setting& cfg,
                                std::string settingName,
                                std::string defaultValue);

/// Parse required array with fixed length from config
template <typename T>
void parseArray(const libconfig::Setting& cfg, std::string settingName,
                T& targetArray) {
  libconfig::Setting& setting = parseSetting(cfg, settingName);
  if (setting.getLength() != targetArray.size()) {
    std::cerr << "Parsing Error in line '" << setting.getSourceLine() << ": "
              << settingName << "' needs to be an array of "
              << targetArray.size() << " integers." << std::endl;
    exit(EXIT_FAILURE);
  }
  for (libconfig::SettingIterator it = setting.begin(); it != setting.end();
       ++it) {
    targetArray[it - setting.begin()] = *it;
  }
}

/// Parse required coordinates from config
Vector3 parseVector(const libconfig::Setting& cfg, std::string settingName);

/// Parse optional vector<size_t> from config
std::vector<size_t> parseOptionalIndexVector(const libconfig::Setting& cfg,
                                             std::string settingName,
                                             size_t maximum);
