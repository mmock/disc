///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "config.h"

/// Parse required setting from config
libconfig::Setting &parseSetting(const libconfig::Setting &cfg,
                                 std::string settingName) {
  if (!cfg.exists(settingName)) {
    std::cerr << "Parsing Error: '" << settingName << "' not found."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  libconfig::Setting &requestedSetting = cfg.lookup(settingName);
  return requestedSetting;
}

/// Parse optional bool from config
bool parseOptionalBool(const libconfig::Setting &cfg, std::string settingName,
                       bool defaultValue) {
  bool result;
  try {
    result = cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    return defaultValue;
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid bool." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse required double from config
double parseDouble(const libconfig::Setting &cfg, std::string settingName) {
  double result;
  try {
    result = cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Parsing Error: '" << nfex.getPath() << "' not found."
              << std::endl;
    exit(EXIT_FAILURE);
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid double." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse optional double from config
double parseOptionalDouble(const libconfig::Setting &cfg,
                           std::string settingName, double defaultValue) {
  double result;
  try {
    result = cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    return defaultValue;
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid double." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse required integer from config
int64_t parseInt(const libconfig::Setting &cfg, std::string settingName) {
  int64_t result;
  try {
    result = cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Parsing Error: '" << nfex.getPath() << "' not found."
              << std::endl;
    exit(EXIT_FAILURE);
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid integer." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse required string from config
std::string parseString(const libconfig::Setting &cfg,
                        std::string settingName) {
  std::string result;
  try {
    result = (const char *)cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    std::cerr << "Parsing Error: '" << nfex.getPath() << "' not found."
              << std::endl;
    exit(EXIT_FAILURE);
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid string." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse optional string from config
std::string parseOptionalString(const libconfig::Setting &cfg,
                                std::string settingName,
                                std::string defaultValue) {
  std::string result;
  try {
    result = (const char *)cfg.lookup(settingName);
  } catch (const libconfig::SettingNotFoundException &nfex) {
    result = defaultValue;
  } catch (const libconfig::SettingTypeException &tpex) {
    std::cerr << "Parsing Error: '" << tpex.getPath()
              << "' value is no valid string." << std::endl;
    exit(EXIT_FAILURE);
  }
  return result;
}

/// Parse required integer from config
Vector3 parseVector(const libconfig::Setting &cfg, std::string settingName) {
  Vector3 result;
  libconfig::Setting &setting = parseSetting(cfg, settingName);
  if (setting.getLength() != 3) {
    std::cerr << "Parsing Error in line '" << setting.getSourceLine() << ": '"
              << settingName << "' needs to be an array of 3 values."
              << std::endl;
    exit(EXIT_FAILURE);
  }
  for (libconfig::SettingIterator it = setting.begin(); it != setting.end();
       ++it) {
    result[it - setting.begin()] = (double)*it;
  }
  return result;
}

/// Parse optional vector<size_t> from config
std::vector<size_t> parseOptionalIndexVector(const libconfig::Setting &cfg,
                                             std::string settingName,
                                             size_t maximum) {
  std::vector<size_t> result;
  if (cfg.exists(settingName)) {
    libconfig::Setting &setting = parseSetting(cfg, settingName);
    for (libconfig::SettingIterator it = setting.begin(); it != setting.end();
         ++it) {
      if ((size_t)*it > maximum) {
        std::cerr << "Parsing Error in line '" << setting.getSourceLine()
                  << ": "
                  << "Index " << (size_t)*it << " exceeds maximum index ("
                  << maximum << ")" << std::endl;
        exit(EXIT_FAILURE);
      }
      result.push_back((size_t)*it);
    }
  }
  return result;
}
