///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"

class Thermo {
 public:
  Thermo(const libconfig::Setting& setting) { parse(setting); }

  ~Thermo() { _thermo.close(); }

  void writeStep(const std::string& line, int step) {
    if (step % _thermoFrequency == 0) {
      _thermo << line;
    }
  }

 private:
  void parse(const libconfig::Setting& setting) {
    MsgLogger(debug) << separatorLine;
    MsgLogger(debug) << "Parsing Thermo:" << std::endl;

    // parse setting
    _thermoFrequency = parseInt(setting, "Frequency");
    MsgLogger(debug) << "Frequency: " << _thermoFrequency << std::endl;
    std::string path = parseString(setting, "FileName");
    MsgLogger(debug) << "FileName: " << path << std::endl;

    _thermo.open(path);
    if (!_thermo.good()) {
      std::cerr << "Error: Unable to create thermo file '" << path << "'"
                << std::endl;
      exit(EXIT_FAILURE);
    }
  }

  std::ofstream _thermo;
  int _thermoFrequency;
};
