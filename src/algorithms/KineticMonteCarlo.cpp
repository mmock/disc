///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#include "KineticMonteCarlo.h"
#include "../events/Event.h"

size_t KMC::randomIndex(size_t containerSize, size_t lowerBound) {
  std::uniform_int_distribution<int> uni(lowerBound, containerSize - 1);
  return uni(_rng);
}

/// Performs a single KMC step.
void KMC::takeStep(const std::vector<Event*>& events) {
  double rateSum = 0;
  double randomNumber;
  _cumulativeRateList.clear();

  // Create cumulative list of events
  assert(events.size() > 0);
  _cumulativeRateList.reserve(events.size());
  for (const auto& event : events) {
    rateSum += event->getRate();
    _cumulativeRateList.push_back(rateSum);
  }

  // Choose one of the events and execute it
  randomNumber = _unitDistribution(_rng) * rateSum;
  auto it = std::lower_bound(_cumulativeRateList.begin(),
                             _cumulativeRateList.end(), randomNumber);

  _lastEvent = events[it - _cumulativeRateList.begin()];
  _lastEvent->execute();

  // update time
  randomNumber = _unitDistribution(_rng);
  _stepDuration = 1 / rateSum * log(1 / randomNumber);
  _time += _stepDuration;

  // Increase step counter
  _steps++;
}

/// Parse config file
void KMC::parse(libconfig::Setting& algorithm) {
  MsgLogger(normal) << separatorLine;
  MsgLogger(normal) << "Parsing Algorithm:" << std::endl;

  // get Steps
  _maximumSteps = parseInt(algorithm, "Steps");
  MsgLogger(normal) << "Steps: " << _maximumSteps << std::endl;
  // get Seed
  _seed = parseInt(algorithm, "Seed");
  _rng.seed(_seed);
  MsgLogger(normal) << "Seed: " << _seed << std::endl;
}
