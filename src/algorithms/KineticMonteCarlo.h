///////////////////////////////////////////////////////////////////////////////
//
//  Copyright (C) 2017, Markus Mock
//
//  This file is part of disc.
//
//  disc is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  disc is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include "../disc.h"

class Event;

class KMC {
 public:
  KMC(libconfig::Setting& cfg) { parse(cfg); };

  double time() const { return _time; }
  double stepDuration() const { return _stepDuration; }
  Event* lastEvent() const { return _lastEvent; }
  int64_t step() const { return _steps; }
  int64_t maximumSteps() const { return _maximumSteps; }

  size_t randomIndex(size_t containerSize, size_t lowerBound = 0);
  void takeStep(const std::vector<Event*>& events);
  void parse(libconfig::Setting& algorithm);

 private:
  Event* _lastEvent;
  std::vector<double> _cumulativeRateList;

  std::default_random_engine _rng;
  std::uniform_real_distribution<> _unitDistribution;
  int64_t _seed;

  int64_t _steps = 0;
  int64_t _maximumSteps;
  double _time = 0;
  double _stepDuration = 0;
};
