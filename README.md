# disc

**disc** is a kinetic monte carlo (KMC) code written for the simulation of
solute diffusion in metals. It is written in C++. At the moment bcc and fcc
lattice structures are supported but extension to other structures is trivial.


If you use **disc** in your research please include the following citation in
publications or presentations:

* M. Mock, and K. Albe <br>
  *Diffusion of yttrium in bcc-iron studied by kinetic Monte Carlo simulations* <br>
  Journal of Nuclear Materials **494**, pp. 157-164 (2017)<br>
  [doi: 10.1016/j.jnucmat.2017.07.021](http://dx.doi.org/10.1016/j.jnucmat.2017.07.021)


## Installation

### Downloading the code

The code is maintained as a public Git repository on
[Gitlab](https://gitlab.com/mmock/disc) You can access it using the `git`
command and copy the repository to your local computer by running:

```
git clone https://gitlab.com/mmock/disc
cd disc
```

This will create a new directory named `disc` on your local
machine containing a copy of the **disc** source files.

### Compiling the code

Building **disc** from source requires the following:

* a C++ compiler,
* the [CMake build system](http://www.cmake.org/),
* the [libconfig library](http://www.hyperrealm.com/libconfig/)
* the [Eigen3 library](http://eigen.tuxfamily.org/)
* the [Boost library](http://www.boost.org/)

On *Ubuntu Linux* these requirements can be readily installed by running:
```
  sudo apt-get install \
    libconfig++-dev \
    libeigen3-dev \
    libboost-dev \
    build-essential \
    cmake
```
To compile the code, create a new subdirectory, typically in the local
directory created above, and invoke CMake:
```
mkdir build
cd build
cmake ..
```
If CMake succeeds in finding all the necessary libraries, it will
create a Makefile. Now the compilation can be launched by
running:
```
make
```

### Compilation options


By default **atomicrex** is compiled as a release version,
which implies that optimizations are included. If this is not
desirable e.g., for debugging purposes, a debug version can be
requested by running:
```
cmake -D CMAKE_BUILD_TYPE=Debug .
```
In this context it can also be useful to turn on verbose output
during compilation, which is accomplished by running `make` as
follows:
```
make VERBOSE=1
```

## Contact

* [Markus Mock](http://www.mawi.tu-darmstadt.de/mm/mm_mm_sw/gruppe_mm_sw_1/mitarbeiterdetails_mm_43520.en.jsp),
  Technische Universität Darmstadt, Germany, mock@mm.tu-darmstadt.de